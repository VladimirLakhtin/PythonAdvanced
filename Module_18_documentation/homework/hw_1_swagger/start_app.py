from flask import Flask
from flask_restx import Api
from werkzeug.serving import WSGIRequestHandler

from Module_18_documentation.homework.hw_1_swagger.controllers import author_controller as a_cont
from Module_18_documentation.homework.hw_1_swagger.controllers import book_controller as b_cont
from Module_18_documentation.homework.hw_1_swagger.models import AUTHORS_DATA, BOOKS_DATA, init_db

app = Flask(__name__)
api = Api(
    app,
    version="2.0",
    title="Sample library API",
    description="An API"
)

api.add_namespace(a_cont.api, path='/')
api.add_namespace(b_cont.api, path='/')


if __name__ == '__main__':
    init_db(BOOKS_DATA, AUTHORS_DATA)
    WSGIRequestHandler.protocol_version = "HTTP/1.1"
    app.run(debug=True)

    # from flask import json
    #
    # with app.app_context(), app.test_request_context():
    #     with open('author_swagger.json', 'w') as f:
    #         f.write(json.dumps(api.__schema__))
