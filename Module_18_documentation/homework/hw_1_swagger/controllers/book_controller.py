from flask import request
from flask_restx import Namespace, fields, Resource
from marshmallow import ValidationError

from Module_18_documentation.homework.hw_1_swagger.controllers.author_controller import author_fields
from Module_18_documentation.homework.hw_1_swagger.models import get_all_books, get_author_by_id, add_book, get_book_by_id, \
    update_book, delete_book_by_id
from Module_18_documentation.homework.hw_1_swagger.schemas import BookSchema, AuthorSchema

api = Namespace('books', description='Books related operations')

book_fields = api.model('Book', {
    'id': fields.Integer,
    'title': fields.String,
    'author': fields.Nested(author_fields),
})
book_create_fields = api.model('CreateBook', {
    'title': fields.String,
    'author_id': fields.Integer,
})


@api.route("api/books", methods=['GET', "POST"])
class BooksList(Resource):
    @api.marshal_with(book_fields, code=200, as_list=True, description="Get books list")
    def get(self):
        schema = BookSchema()
        return schema.dump(get_all_books(), many=True)

    @api.expect(book_create_fields)
    @api.marshal_with(book_fields, code=201, description="Book created")
    @api.response(400, "Validation error")
    def post(self):
        data = request.json
        schema = BookSchema()

        try:
            data['author'] = get_author_by_id(data['author_id'])
            if not data['author']:
                return "{'author_id': ['No author with such id']}", 400
            del data['author_id']
            data['author'] = AuthorSchema().dump(data['author'])
            if not data['author']['middle_name']:
                del data['author']['middle_name']
            book = schema.load(data)
        except ValidationError as e:
            return e.messages, 400

        book = add_book(book)
        return schema.dump(book), 201


@api.route("api/books/<int:id>")
class BookDetails(Resource):
    @api.marshal_with(book_fields, code=200, description="Get book by id")
    def get(self, id):
        schema = BookSchema()
        book = get_book_by_id(id)
        if book:
            return schema.dump(book)
        return '', 404

    @api.expect(book_create_fields)
    @api.marshal_with(book_fields, code=200, description="Updated book by id")
    @api.response(400, "Validation error")
    def put(self, id):
        data = request.json
        schema = BookSchema()

        try:
            data['author'] = get_author_by_id(data['author_id'])
            if not data['author']:
                return "{'author_id': ['No author with such id']}", 400
            del data['author_id']
            data['author'] = AuthorSchema().dump(data['author'])
            if not data['author']['middle_name']:
                del data['author']['middle_name']
            data['id'] = id
            book = schema.load(data)
        except ValidationError as e:
            return e.messages, 400

        book = update_book(book)
        return schema.dump(book), 201

    @api.response(200, "Deleted author")
    def delete(self, id):
        if get_book_by_id(id):
            delete_book_by_id(id)
            return '', 200
        return '', 404
