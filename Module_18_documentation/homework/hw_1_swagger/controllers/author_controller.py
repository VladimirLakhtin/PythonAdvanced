from flask import request
from flask_restx import Namespace, fields, Resource
from marshmallow import ValidationError

from Module_18_documentation.homework.hw_1_swagger.models import (
    get_all_author,
    add_author,
    get_author_by_id,
    get_all_book_by_author_id,
    delete_author_by_id,
)
from Module_18_documentation.homework.hw_1_swagger.schemas import AuthorSchema, BookSchema

api = Namespace('authors', description='Authors related operations')

author_fields = api.model('Author', {
    'id': fields.Integer,
    'first_name': fields.String,
    'last_name': fields.String,
    'middle_name': fields.String,
})
author_create_fields = api.model('CreateAuthor', {
    'first_name': fields.String,
    'last_name': fields.String,
    'middle_name': fields.String,
})


@api.route("api/authors")
class AuthorList(Resource):
    @api.marshal_with(author_fields, code=200, description="Get authors list")
    def get(self):
        schema = AuthorSchema()
        return schema.dump(get_all_author(), many=True)

    @api.expect(author_create_fields)
    @api.marshal_with(author_fields, code=201, description="Created author")
    @api.response(400, "Validation error")
    def post(self):
        data = request.json
        schema = AuthorSchema()

        try:
            author = schema.load(data)
        except ValidationError as e:
            return e.messages, 400

        author = add_author(author)
        return schema.dump(author), 201


@api.route("api/authors/<int:id>")
class AuthorDetails(Resource):
    @api.marshal_with(author_fields, code=200, description="Get author's books by id", as_list=True)
    def get(self, id: int):
        schema = BookSchema()
        author = get_author_by_id(id)
        if author:
            books = get_all_book_by_author_id(id)
            return schema.dump(books, many=True)
        return '', 404

    @api.response(200, description="Deleted author")
    def delete(self, id: int):
        if get_author_by_id(id):
            delete_author_by_id(id)
            return '', 200
        return '', 404
