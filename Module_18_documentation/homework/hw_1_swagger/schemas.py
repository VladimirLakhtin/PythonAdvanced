from marshmallow import Schema, fields, validates, ValidationError, post_load

from Module_17_REST_API.homework.models import get_book_by_title, Book, Author, get_author_by_id


class AuthorSchema(Schema):

    id = fields.Int()
    first_name = fields.Str(required=True)
    last_name = fields.Str(required=True)
    middle_name = fields.Str(required=False)

    @post_load
    def create_author(self, data, **kwargs) -> Author:
        return Author(**data)


class BookSchema(Schema):

    id = fields.Int()
    title = fields.Str(required=True)
    author = fields.Nested(AuthorSchema)

    @validates('title')
    def validate_title(self, title: str) -> None:
        if get_book_by_title(title) is not None:
            raise ValidationError(
                f"Book with title '{title}' already exists"
                f"please use a different title"
            )

    @post_load
    def create_book(self, data, **kwargs) -> Book:
        return Book(**data)
