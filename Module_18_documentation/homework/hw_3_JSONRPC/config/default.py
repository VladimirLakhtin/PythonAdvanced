import os

# Конфигурация
DEBUG = True
SQLDEBUG = False

SESSION_COOKIE_NAME = 'myapp'
SESSION_TYPE = 'filesystem'

TITLE = 'Проект'

DIR_BASE = '/'.join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1])
# Генерировать можно утилитой pwgen
# Пример:
# pwgen -sy 64
SECRET_KEY = '''0123456789'''
