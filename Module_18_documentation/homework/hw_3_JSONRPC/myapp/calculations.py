from flask_jsonrpc.exceptions import InvalidParamsError

from myapp import jsonrpc


@jsonrpc.method('calculations.adding')
def adding(a: float, b: float) -> float:
    return a + b


@jsonrpc.method('calculations.subtraction')
def subtraction(a: float, b: float) -> float:
    return a - b


@jsonrpc.method('calculations.multiplication')
def multiplication(a: float, b: float) -> float:
    return a * b


@jsonrpc.method('calculations.division')
def division(a: float, b: float) -> float:
    if b == 0:
        raise InvalidParamsError("Argument 'b' cannot be a zero")
    return a / b
