import config

from flask import Flask
from flask_jsonrpc import JSONRPC

app = Flask(__name__)
app.config.from_object(config.CONFIG)

jsonrpc = JSONRPC(app, '/api')

# API
from . import calculations
