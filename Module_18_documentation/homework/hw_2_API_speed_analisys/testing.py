import logging
import multiprocessing
import time
from multiprocessing.pool import ThreadPool

from Module_18_documentation.homework.hw_2_API_speed_analisys.client import BookClient

client = BookClient()


logging.basicConfig(
    format="%(levelname)s | %(funcName)s | %(message)s",
    level=logging.INFO
)
logger = logging.getLogger()


def run_testing_synchronously(requests_count):
    start = time.time()
    for _ in range(requests_count):
        client.get_all_books_session()
    logger.info(f"Done {requests_count} requests in {time.time() - start:0.4} sec")


def run_testing_multithreading(requests_count):
    start = time.time()
    with ThreadPool(processes=multiprocessing.cpu_count() * 5) as pool:
        for _ in range(requests_count):
            pool.apply_async(client.get_all_books_session)
    logger.info(f"Done {requests_count} requests in {time.time() - start:0.4} sec")


if __name__ == '__main__':
    for i in range(1, 4):
        run_testing_synchronously(10 ** i)
        run_testing_multithreading(10 ** i)
