import requests
import typing as tp


class BookClient:
    URL = 'http://127.0.0.1:5000/api/books'
    TIMEOUT = 5

    def __init__(self):
        self._session = requests.Session()

    def get_all_books_session(self) -> tp.Dict:
        response = self._session.get(self.URL, timeout=self.TIMEOUT)
        return response.json()

    def add_new_book_session(self, data: tp.Dict) -> tp.Optional[tp.Dict]:
        response = self._session.post(self.URL, json=data, timeout=self.TIMEOUT)
        if response.status_code != 201:
            raise ValueError(f"Wrong params. Response message: {response.json()}")
        return response.json()

    def get_all_books(self) -> tp.Dict:
        response = requests.get(self.URL, timeout=self.TIMEOUT)
        return response.json()

    def add_new_book(self, data: tp.Dict) -> tp.Optional[tp.Dict]:
        response = requests.post(self.URL, json=data, timeout=self.TIMEOUT)
        if response.status_code != 201:
            raise ValueError(f"Wrong params. Response message: {response.json()}")
        return response.json()
