import logging

from flask import Flask, request


FILENAME = "server/logs.log"
template = "{levelname} | {name} | {asctime} | {lineno} | {message}"

app = Flask(__name__)

logger = logging.getLogger(__name__)


@app.route('/log', methods=['GET', 'POST'])
def log():
    if request.method == "GET":
        with open(FILENAME) as f:
            logs_list = f.read().splitlines()
        return logs_list, 200
    if request.method == "POST":
        data = request.form
        log = template.format(**data)
        with open(FILENAME, 'a') as f:
            f.write(log + '\n')
        return 'Ok', 200


if __name__ == '__main__':
    app.config['DEBUG'] = True
    app.run()
