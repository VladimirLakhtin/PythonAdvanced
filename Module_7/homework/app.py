import logging
import logging.config
import sys

from logging_tree import printout

from Module_7.homework.inifile_to_dict.main import dict_config
from utils import string_to_operator


def logging_config():
    # fmt = "%(levelname)s | %(name)s | %(asctime)s | %(lineno)s | %(message)s"
    # formatter = logging.Formatter(fmt=fmt)
    # handler = CalcFileHandler()
    # handler.setFormatter(formatter)
    # logging.basicConfig(
    #     handlers=[handler],
    #     level=logging.DEBUG,
    # )
    logging.config.dictConfig(dict_config)


logger = logging.getLogger('appLogger')


def calc(args):
    logger.debug(f"Arguments: {args}")

    num_1 = args[0]
    operator = args[1]
    num_2 = args[2]

    try:
        num_1 = float(num_1)
    except ValueError as e:
        logger.error("Error while converting number 1")
        logger.error(e)

    try:
        num_2 = float(num_2)
    except ValueError as e:
        logger.error("Error while converting number 2")
        logger.error(e)

    operator_func = string_to_operator(operator)

    result = operator_func(num_1, num_2)

    logger.debug(f"Result: {result}")
    logger.info(f"{num_1} {operator} {num_2} = {result}")


if __name__ == '__main__':
    logging_config()
    calc((1, '/', 3))
    # printout()
