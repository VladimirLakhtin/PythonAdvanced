import logging
from logging.handlers import TimedRotatingFileHandler

LOG_ROTATE = 10
BACKUP = 5
LOG_HOST = '127.0.0.1:5000'
LOG_URL = '/log'


class CalcFileHandler(logging.Handler):
    def __init__(self, mode='a'):
        super().__init__()
        self.file_name = 'logs/calc_{}.log'
        self.mode = mode

    def emit(self, record: logging.LogRecord) -> None:
        message = self.format(record)
        level_name = record.levelname.lower()
        with open(self.file_name.format(level_name), self.mode) as f:
            f.write(message + '\n')


class ASCIIFilter(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        return record.msg.isascii()


dict_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "is_ascii": {
            "()": ASCIIFilter,
        },
    },
    "formatters": {
        "base": {
            "format": "%(levelname)s | %(name)s | %(asctime)s | %(lineno)s | %(message)s",
        },
    },
    "handlers": {
        "app_files": {
            "()": CalcFileHandler,
            "level": "DEBUG",
            "formatter": "base",
            "mode": "a",
            "filters": ["is_ascii"]
        },
        "utils_file": {
            "class": "logging.handlers.TimedRotatingFileHandler",
            "level": "INFO",
            "formatter": "base",
            "filename": "logs/utils.log",
            "when": "h",
            "interval": LOG_ROTATE,
            "backupCount": BACKUP,
        },
        "server": {
            "class": "logging.handlers.HTTPHandler",
            "level": "DEBUG",
            "host": LOG_HOST,
            "url": LOG_URL,
            "method": "POST"
        }
    },
    "loggers": {
        "app": {
            "level": "DEBUG",
            "handlers": ["app_files", "server"],
        },
        "utils": {
            "level": "DEBUG",
            "handlers": ["utils_file", "server"],
        },
    },
}


