import logging
import sys


class CustomStreamHandler(logging.Handler):
    def __init__(self, thread=None):
        super().__init__()
        self.thread = thread or sys.stderr

    def emit(self, record: logging.LogRecord) -> None:
        message = self.format(record)
        self.thread.write(message + '\n')


dict_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "base": {
            "format": "%(name)s || %(levelname)s || %(message)s || %(module)s.%(funcName)s:%(lineno)d"
        },
    },
    "handlers": {
        "console_base": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "base",
        },
        "console_sub_1": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "base",
        },
        "file": {
            "class": "logging.FileHandler",
            "level": "DEBUG",
            "formatter": "base",
            "filename": "logfile.log",
            "mode": "a",
        },
        "custom_base": {
            "()": CustomStreamHandler,
            "level": "DEBUG",
            "formatter": "base",
        }
    },
    "loggers": {
        "": {
            "level": 'WARNING',
            "handlers": ["custom_base"],
        },
        "sub_1": {
            "level": "INFO",
            "handlers": ["custom_base", "file"],
        },
        "sub_2": {
            "level": "INFO",
            "handlers": ["file"],
            "propagate": False,
        },
        "sub_1.sub_sub_1": {
            "level": "DEBUG",
            "propagate": True,
        },
    },
}
