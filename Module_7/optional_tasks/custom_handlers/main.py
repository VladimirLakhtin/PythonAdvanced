import logging
from logging import config
from logger_config import dict_config


logging.config.dictConfig(dict_config)

logger = logging.getLogger()
sub_1_logger = logging.getLogger('sub_1')
sub_2_logger = logging.getLogger('sub_2')
sub_sub_1_logger = logging.getLogger('sub_1.sub_sub_1')


def main():
    print(logger)
    print(logger.handlers)
    print(sub_1_logger)
    print(sub_1_logger.handlers)
    print(sub_2_logger)
    print(sub_2_logger.handlers)
    print(sub_sub_1_logger)
    print(sub_sub_1_logger.handlers)

    sub_1_logger.info('Hi')
    sub_sub_1_logger.debug('Hello')
    sub_2_logger.warning('Hey')


if __name__ == '__main__':
    main()
