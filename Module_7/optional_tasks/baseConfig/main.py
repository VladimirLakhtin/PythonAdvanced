import logging


logger = logging.getLogger()
logging.basicConfig()

sub_1_logger = logging.getLogger('sub_1')
sub_1_logger.setLevel('INFO')

sub_2_logger = logging.getLogger('sub_2')
sub_2_logger.propagate = False

sub_sub_1_logger = logging.getLogger('sub_1.sub_sub_1')
sub_sub_1_logger.setLevel('DEBUG')


custom_handler_sub_1 = logging.StreamHandler()
fmt = "%(name)s || %(levelname)s || %(message)s || %(module)s.%(funcName)s:%(lineno)d"
formatter = logging.Formatter(fmt=fmt)

custom_handler_sub_1.setLevel('DEBUG')
custom_handler_sub_1.setFormatter(formatter)
sub_1_logger.addHandler(custom_handler_sub_1)

custom_handler_root = logging.StreamHandler()
custom_handler_root.setLevel('DEBUG')
custom_handler_root.setFormatter(formatter)
logger.handlers = [custom_handler_root]


def main():
    print(logger)
    print(logger.handlers)
    print(sub_1_logger)
    print(sub_1_logger.handlers)
    print(sub_2_logger)
    print(sub_2_logger.handlers)
    print(sub_sub_1_logger)
    print(sub_sub_1_logger.handlers)

    sub_1_logger.info('Hi')
    sub_sub_1_logger.debug('Hello')
    sub_2_logger.warning('Hey')


if __name__ == '__main__':
    main()