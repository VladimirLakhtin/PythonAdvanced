dict_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "base": {
            "format": "%(name)s || %(levelname)s || %(message)s || %(module)s.%(funcName)s:%(lineno)d"
        },
    },
    "handlers": {
        "console_base": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "base",
        },
        "console_sub_1": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "base",
        },
        "file": {
            "class": "logging.FileHandler",
            "level": "DEBUG",
            "formatter": "base",
            "filename": "logfile.log",
            "mode": "a",
        },
    },
    "loggers": {
        "": {
            "level": 'WARNING',
            "handlers": ["console_base"],
        },
        "sub_1": {
            "level": "INFO",
            "handlers": ["console_sub_1", "file"],
        },
        "sub_2": {
            "level": "INFO",
            "handlers": ["file"],
            "propagate": False,
        },
        "sub_1.sub_sub_1": {
            "level": "DEBUG",
            "propagate": True,
        },
    },
}
