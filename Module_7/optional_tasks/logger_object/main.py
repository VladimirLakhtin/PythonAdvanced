import logging


logger = logging.getLogger()
main_logger = logging.getLogger('main')
main_logger.setLevel('INFO')
utils_logger = logging.getLogger('main.utils')
utils_logger.setLevel('DEBUG')


def main():
    print(logger)
    print(main_logger)
    print(utils_logger)


if __name__ == '__main__':
    main()