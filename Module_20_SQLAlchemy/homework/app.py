from flask import Flask
from flask_restx import Api

from routes import ns

app = Flask(__name__)
api = Api(
    app,
    version="2.0",
    title="Sample library API",
    description="An API"
)

api.add_namespace(ns, path='/api')

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000)
