from flask import request
from flask_restx import Namespace, Resource
from marshmallow import ValidationError

from Module_20_SQLAlchemy.homework.models import Book, ReceivingBooks
from Module_20_SQLAlchemy.homework.schemas import IssueReceivingBooksSchema, ReturnReceivingBooksSchema

ns = Namespace('library', description='Authors related operations')


@ns.route('/books', methods=['GET'])
class BooksResource(Resource):
    def get(self):
        books_data = [
            book.to_json()
            for book in Book.get_all_books()
        ]
        return {"books": books_data}, 200


@ns.route('/library/debtors', methods=['GET'])
class CountDebtorsResource(Resource):
    def get(self):
        return {'count_debtors': ReceivingBooks.get_debtors_count()}


@ns.route('/books/issue', methods=['POST'])
class IssueBookResource(Resource):
    def post(self):
        schema = IssueReceivingBooksSchema()
        data = request.json
        try:
            schema.load(data)
        except ValidationError as e:
            return e.messages_dict, 400
        return 'Ok', 200


@ns.route('/books/return', methods=['POST'])
class ReturnBookResource(Resource):
    def post(self):
        schema = ReturnReceivingBooksSchema()
        data = request.json
        try:
            schema.load(data)
        except ValidationError as e:
            return e.messages_dict, 400
        return 'Ok', 200
