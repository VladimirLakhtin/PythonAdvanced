import datetime
from typing import List, Any, Dict

from sqlalchemy import create_engine, Column, Integer, Text, Date, Float, DateTime, Boolean, update, and_, func
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import sessionmaker, declarative_base

engine = create_engine('sqlite:///lirary.db')
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()

BOOKS_DATA = [
    {
        "name": "A Byte of Python",
        "realize_date": datetime.datetime.now(),
        "count": 2,
        "author_id": 3,
    },
    {
        "name": "Capitan's Daughter",
        "realize_date": datetime.datetime.now(),
        "count": 5,
        "author_id": 2,
    },
    {
        "name": "Tom Sawyer",
        "realize_date": datetime.datetime.now(),
        "count": 3,
        "author_id": 1,
    },
]

AUTHORS_DATA = [
    {
        "name": "Mark",
        "surname": "Twain",
    },
    {
        "name": "Alexandr",
        "surname": "Pushkin",
    },
    {
        "name": "Swaroop",
        "surname": "C",
    },
]

STUDENTS_DATA = [
    {
        "name": "Vladik",
        "surname": "Pupkin",
        "phone": "+79999999999",
        "email": "p@gmail.com",
        "average_score": 4.5,
        "scholarship": True,
    },
    {
        "name": "Vovchik",
        "surname": "Petrov",
        "phone": "+79998888888",
        "email": "v@gmail.com",
        "average_score": 3.8,
        "scholarship": False,
    },
    {
        "name": "Alina",
        "surname": "Ivanova",
        "phone": "+79997777777",
        "email": "a@gmail.com",
        "average_score": 4.9,
        "scholarship": True,
    },
]


class Author(Base):
    __tablename__ = "author"

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    surname = Column(Text, nullable=False)


class Book(Base):
    __tablename__ = "book"

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    count = Column(Integer, default=1)
    realize_date = Column(Date, nullable=False)
    author_id = Column(Integer, nullable=False)

    def to_json(self) -> Dict[str, Any]:
        book_data = {c.name: getattr(self, c.name)
                     for c in self.__table__.columns
                     if c.name != 'realize_date'}
        book_data["realize_date"] = str(self.realize_date)
        return book_data

    @classmethod
    def get_all_books(cls):
        return session.query(Book).all()


class Student(Base):
    __tablename__ = "student"

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    surname = Column(Text, nullable=False)
    phone = Column(Text, nullable=False)
    email = Column(Text, nullable=False)
    average_score = Column(Float, nullable=False)
    scholarship = Column(Boolean, nullable=False)

    @classmethod
    def get_students_with_scholarship(cls) -> List[Base]:
        return session.query(cls).filter(cls.scholarship).all()

    @classmethod
    def get_students_with_score_above(cls, score: float) -> List[Base]:
        return session.query(cls).filter(cls.average_score > score).all()


class ReceivingBooks(Base):
    __tablename__ = "receiving_books"

    id = Column(Integer, primary_key=True)
    book_id = Column(Integer, nullable=False)
    student_id = Column(Integer, nullable=False)
    date_of_issue = Column(DateTime, nullable=False,
                           default=datetime.datetime.now())
    date_of_return = Column(DateTime, nullable=True)

    @hybrid_property
    def count_date_with_book(self) -> int:
        date_end = self.date_of_return or datetime.datetime.now()
        return (date_end - self.date_of_issue).day

    @classmethod
    def get_debtors_count(cls) -> int:
        expiration_date = datetime.datetime.now() - datetime.timedelta(days=14)
        return session \
            .query(func.count(func.distinct(cls.student_id))) \
            .filter(cls.date_of_return.is_(None)) \
            .filter(cls.date_of_issue < expiration_date) \
            .scalar()

    @classmethod
    def issue_book(cls, book_id: int, student_id: int) -> None:
        session.add(cls(book_id=book_id, student_id=student_id))
        book = session.query(Book).filter(Book.id == book_id).one()
        book.count -= 1
        session.commit()

    @classmethod
    def return_book(cls, book_id: int, student_id: int) -> None:
        query = update(cls) \
            .where(cls.book_id == book_id) \
            .where(cls.student_id == student_id) \
            .values(date_of_return=datetime.datetime.now())
        session.execute(query)
        book = session.query(Book).filter(Book.id == book_id).one()
        book.count += 1
        session.commit()


Base.metadata.create_all(engine)

if __name__ == '__main__':
    if not session.query(Author).all():
        for author_data in AUTHORS_DATA:
            new_author = Author(name=author_data['name'], surname=author_data["surname"])
            session.add(new_author)

    if not session.query(Book).all():
        for book_data in BOOKS_DATA:
            new_book = Book(name=book_data['name'], count=book_data["count"],
                            realize_date=book_data["realize_date"],
                            author_id=book_data["author_id"])
            session.add(new_book)

    if not session.query(Student).all():
        for student_data in STUDENTS_DATA:
            new_student = Student(name=student_data['name'], surname=student_data["surname"],
                                  phone=student_data["phone"], email=student_data["email"],
                                  average_score=student_data["average_score"],
                                  scholarship=student_data["scholarship"])
            session.add(new_student)

    session.commit()
