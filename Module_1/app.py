import os
import random
import re
from datetime import datetime, timedelta

from flask import Flask


app = Flask(__name__)
cars = ['Chevrolet', 'Renault', 'Ford', 'Lada']
cats = ['корниш-рекс', 'русская голубая', 'шотландская вислоухая', 'мейн-кун', 'манчкин']

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
BOOK_FILE = os.path.join(BASE_DIR, 'war_and_peace.txt')
with open(BOOK_FILE, 'r', encoding='utf-8') as file:
    text = file.read()
words = re.findall(r'[a-zA-Zа-яА-Я]+', text)


@app.route('/hello_world')
def hello_function():
    return 'Привет, мир!'


@app.route('/cars')
def get_cars():
    return ', '.join(cars)


@app.route('/cats')
def get_random_cat():
    return random.choice(cats)


@app.route('/get_time/now')
def get_current_time():
    now = datetime.now()
    current_time = now.strftime('%H:%M:%S')
    return f'Точное время: {current_time}'


@app.route('/get_time/future')
def get_future_time():
    after_hour = datetime.now() + timedelta(hours=1)
    current_time_after_hour = after_hour.strftime('%H:%M:%S')
    return f'Точное время через час будет {current_time_after_hour}'


@app.route('/get_random_word')
def get_random_word():
    random_word = random.choice(words)
    return random_word


@app.route('/counter')
def get_counter():
    get_counter.visits += 1
    return str(get_counter.visits)


get_counter.visits = 0
