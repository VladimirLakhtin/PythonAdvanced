import logging
import random
import threading
import time

TOTAL_TICKETS = 0
SEATING = 30

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Seller(threading.Thread):

    def __init__(self, semaphore: threading.Semaphore):
        super().__init__()
        self.sem = semaphore
        self.tickets_sold = 0
        logger.info('Seller started work')

    def run(self):
        global TOTAL_TICKETS
        is_running = True
        while is_running:
            self.random_sleep()
            with self.sem:
                if TOTAL_TICKETS <= 0:
                    break
                self.tickets_sold += 1
                TOTAL_TICKETS -= 1
                logger.info(f'{self.name} sold one;  {TOTAL_TICKETS} left')

        logger.info(f'Seller {self.name} sold {self.tickets_sold} tickets')

    @staticmethod
    def random_sleep():
        time.sleep(random.randint(1, 2))


class Director(threading.Thread):
    def __init__(self, semaphore: threading.Semaphore, count_sellers: int):
        super().__init__()
        self.sem = semaphore
        self.count_sellers = count_sellers
        self.issued_tickets = 0
        self.tickets_batch_size = 10 - count_sellers
        logger.info('Director started work')

    def run(self):
        global TOTAL_TICKETS, SEATING
        is_running = True
        while is_running:
            if TOTAL_TICKETS <= self.count_sellers:
                available_seats = SEATING - self.issued_tickets
                with self.sem:
                    batch_size = a if (t := self.tickets_batch_size) > (a := available_seats) else t
                    TOTAL_TICKETS += batch_size
                    self.issued_tickets += batch_size
                    logger.info(f'Director {self.name} issue {batch_size} tickets; TOTAL_TICKETS = {TOTAL_TICKETS}')
                    if SEATING - self.issued_tickets == 0:
                        break

        logger.info(f"tickets are out. Director {self.name} issued {self.issued_tickets} tickets")


def main():
    semaphore = threading.Semaphore()
    sellers_count = 3
    sellers = []

    director = Director(semaphore, sellers_count)
    director.start()
    for _ in range(sellers_count):
        seller = Seller(semaphore)
        seller.start()
        sellers.append(seller)

    director.join()
    for seller in sellers:
        seller.join()


if __name__ == '__main__':
    main()
