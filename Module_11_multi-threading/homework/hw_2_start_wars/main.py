import datetime
import json
import logging
import multiprocessing
import sqlite3
import threading
import time
from typing import Dict

import requests


URL = "https://swapi.dev/api/people/{}"

logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(funcName)s: %(message)s")
logger = logging.getLogger(__name__)


def calculate_age(birth_year):
    birth_year = 1977 + (-1 if birth_year[-3:] == "BBY" else 1) * int(float(birth_year[:-3]))
    return datetime.datetime.now().year - birth_year


def get_character_info(char_id: int) -> Dict:
    logger.info(f"URL: {URL.format(char_id)}")
    try:
        response = requests.get(URL.format(char_id), timeout=10)
    except requests.exceptions.ReadTimeout:
        logger.error("Timeout error")
        return {}
    logger.debug(f"Response text: {response.text}")
    data = json.loads(response.text)
    logger.debug(f"Response data: {data}")

    if response.status_code == 200:
        result = {
            "name": data["name"],
            "age": calculate_age(by) if "BY" in (by := data["birth_year"]) else None,
            "sex": g if 'male' in (g := data['gender']) else None,
        }
        logger.debug(f"Output data: {result}")
        return result
    logger.error(f"Response status: {response.status_code}. Data: {data}")
    return {}


def add_record(data: Dict) -> None:
    conn = sqlite3.connect('db.db', check_same_thread=False)
    cur = conn.cursor()

    data = {
        k: str(v) if k == 'age' else f"'{v}'"
        for k, v in data.items()
        if v
    }
    sql = f"""INSERT OR REPLACE INTO characters({','.join(data.keys())})
              VALUES({','.join(data.values())}) """
    logger.debug(f"Insert SQL query: {sql}")
    try:
        cur.execute(sql)
        conn.commit()
        logger.info("New record insert into database")
    except Exception as e:
        logger.error(str(e))


def load_character(char_id) -> None:
    chars_data = get_character_info(char_id)
    if chars_data:
        add_record(chars_data)


# 39.55 sec
def loads_characters_synchrony() -> None:
    logger.info("Start loads characters")
    start = time.time()
    for char_id in range(1, 21):
        load_character(char_id)

    logger.info(f"Done in {time.time() - start:0.4}")


# 10.41
def loads_characters_multithreading() -> None:
    start = time.time()
    threads = []
    for char_id in range(1, 21):
        thread = threading.Thread(target=load_character, kwargs={'char_id': char_id})
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

    logger.info(f"Done in {time.time() - start:0.4}")


# 7.76
def loads_characters_multiprocessing() -> None:
    start = time.time()
    processes = []
    for char_id in range(1, 21):
        process = multiprocessing.Process(target=load_character, kwargs={'char_id': char_id})
        process.start()
        processes.append(process)

    for process in processes:
        process.join()

    logger.info(f"Done in {time.time() - start:0.4}")


if __name__ == "__main__":

    loads_characters_multiprocessing()
