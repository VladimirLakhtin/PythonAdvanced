import logging
import queue
import random
import threading


logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(message)s")
logger = logging.getLogger(__name__)


class Producer(threading.Thread):
    def __init__(self, q: queue.PriorityQueue, count_tasks: int = 10):
        super().__init__()
        self.q = q
        self.count_tasks = count_tasks

    def run(self):
        logger.info(f"{self.__class__.__name__}: Running")
        for _ in range(self.count_tasks):
            priority = random.randint(1, 5)
            task = f'sleep({random.random():0.5})'
            self.q.put((priority, task))
        self.q.join()
        logger.info(f"{self.__class__.__name__}: Done")

    def get_queue(self):
        return self.q


class Consumer(threading.Thread):
    def __init__(self, q: queue.PriorityQueue):
        super().__init__()
        self.q = q

    def run(self):
        logger.info(f"{self.__class__.__name__}: Running")
        while not self.q.empty():
            task = self.q.get()
            logger.info(f'>running Task(priority={task[0]}). {task[1]}')
            self.q.task_done()
        self.q.join()
        logger.info(f"{self.__class__.__name__}: Done")


def main():
    q = queue.PriorityQueue()
    producer = Producer(q)
    producer.start()

    consumer = Consumer(q)
    consumer.start()

    producer.join()
    consumer.join()


if __name__ == "__main__":
    main()
