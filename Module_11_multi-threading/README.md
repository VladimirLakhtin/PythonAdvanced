# Мультипоточность

### Определения - https://habr.com/ru/articles/337528/

| Характеристика                   | Процесс                                                                  | Поток                                                         | 
|----------------------------------|--------------------------------------------------------------------------|---------------------------------------------------------------|
| Сущность                         | Содержит потоки                                                          | Наименьшая единица программы. Существуют внутри процесса      |
| Коммуникация                     | Не делят одно адресное пространство. Коммникация сложная и дорогостоящая | Делят адресное пространство одного процесса                   |
| Создание                         | Дорогое                                                                  | Простое                                                       |
| Безопасность                     | Безопасны                                                                | Небезопасны, ввиду того, что делят одно адресное пространство |
| Параллельное выполнение в Python | Возможно                                                                 | Невозможно ввиду GIL                                          |


