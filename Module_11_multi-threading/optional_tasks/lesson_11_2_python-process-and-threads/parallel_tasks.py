import logging
import multiprocessing
import threading
import time

import requests


logging.basicConfig(level=logging.INFO)
loger = logging.getLogger(__name__)


URL = "https://cataas.com/cat"
OUT_PATH = 'temp/{}.jpeg'


def get_image(url: str, result_path: str) -> None:
    response = requests.get(url, timeout=(5, 5))
    if response.status_code != 200:
        return
    with open(result_path, 'wb') as ouf:
        ouf.write(response.content)


def load_images_base(count: int) -> None:
    start = time.time()
    for i in range(count):
        get_image(URL, OUT_PATH.format(i))

    loger.info(f'[Base] For {count} images done in {time.time() - start:.4}')



def load_images_multithreading(count: int) -> None:
    start = time.time()
    threads = []
    for i in range(count):
        thread = threading.Thread(target=get_image, args=(URL, OUT_PATH.format(i)))
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

    loger.info(f'[Multithreading] For {count} images done in {time.time() - start:.4}')


def load_images_multiprocessing(count: int) -> None:
    start = time.time()
    processes = []

    for i in range(count):
        process = multiprocessing.Process(target=get_image, args=(URL, OUT_PATH.format(i)))
        process.start()
        processes.append(process)

    for process in processes:
        process.join()

    loger.info(f'[Multiprocessing] For {count} images done in {time.time() - start:.4}')


if __name__ == '__main__':
    for i in range(10, 100, 10):
        load_images_base(i)
        load_images_multithreading(i)
        load_images_multiprocessing(i)
