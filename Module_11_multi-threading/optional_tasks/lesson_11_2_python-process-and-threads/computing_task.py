import logging
import multiprocessing
import threading
import time


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def task(number):
    return sum(i ** i for i in range(number))


def run_base(count):
    start = time.time()
    for i in range(count):
        task(i)

    logger.info(f'[Base] For {count} tasks done in {time.time() - start:0.4}')


def run_multithreading(count):
    start = time.time()
    threads = []
    for i in range(count):
        thread = threading.Thread(target=task, kwargs={'number': i})
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

    logger.info(f'[Multithreading] For {count} tasks done in {time.time() - start:0.4}')


def run_multiprocessing(count):
    start = time.time()
    processes = []
    for i in range(count):
        process = multiprocessing.Process(target=task, kwargs={'number': i})
        process.start()
        processes.append(process)

    for process in processes:
        process.join()

    logger.info(f'[Multiprocessing] For {count} tasks done in {time.time() - start:0.4}')


if __name__ == '__main__':
    for i in range(100, 1000, 100):
        run_base(i)
        run_multithreading(i)
        # run_multiprocessing(i)
