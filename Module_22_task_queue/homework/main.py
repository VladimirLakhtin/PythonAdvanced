import json
import logging
from typing import List

import yagmail
from celery.schedules import crontab
from flask import Flask, request
from PIL import Image, ImageFilter
from celery import Celery, chord

import config

app = Flask(__name__)

celery_app = Celery(
    app.name,
    broker='redis://localhost:6379/0',
    backend='redis://localhost:6379/0',
)

yag = yagmail.SMTP(config.SENDER_EMAIL, config.PASSWORD)

logger = logging.getLogger(__name__)
logger.setLevel('DEBUG')


@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs) -> None:
    sender.add_periodic_task(
        crontab(hour=12, minute=0, day_of_week=1),
        send_info.s(),
        name='send_info'
    )


@celery_app.task
def send_info() -> None:
    with open('emails.json') as file:
        emails = json.load(file)
    for email in emails:
        yag.send(
            to=email,
            subject=config.SUBJECTS_INFO_TEXT,
            contents=config.INFO_MESSAGE,
        )


@celery_app.task()
def process_image_file(img_path: str) -> str:
    img = Image.open(img_path)
    blur_img = img.filter(ImageFilter.BLUR)
    blur_img.save(img_path)
    return img_path


@celery_app.task()
def send_image(images: List) -> None:
    yag.send(
        to=config.receiver_email,
        subject=config.SUBJECT_TEXT,
        contents=config.BODY_TEXT,
        attachments=images,
    )


@app.route('/blur', methods=['POST'])
def blur():
    images_files = request.files.getlist('images')
    tasks, errors = [], []

    if not all(
        img_file.filename.endswith(('.jpeg', '.png', '.jpg'))
        for img_file in images_files
    ):
        return {"error": "Files must have extensions: .jpg, .jpeg, .png"}, 400

    for image_file in images_files:
        img_path = config.IMAGES_PATH / image_file.filename
        image_file.save(img_path)
        tasks.append(process_image_file.s(str(img_path)))

    tasks = chord(tasks)(send_image.s())
    return {"task_id": tasks.id}, 200


@app.route('/status/<string:id>', methods=['GET'])
def get_status(id: str):
    # TODO: add status 'Processed'

    result = celery_app.AsyncResult(id)
    if result:
        status = "Send" if result.ready() else "In process"
        return {'status': status}, 200
    else:
        return {'error': 'Invalid group_id'}, 404


@app.route('/subscribe', methods=['POST'])
def subscribe():
    email = request.json.get('email')
    if email:
        with open("emails.json") as file:
            emails: List = json.load(file)
        emails.append(email)
        with open("emails.json", 'w') as file:
            json.dump(emails, file, indent=4)
        return {"subscribe": "success"}, 200
    return {"error": "No email in body"}, 400


@app.route('/unsubscribe', methods=['POST'])
def unsubscribe():
    email = request.json.get('email')
    if not email:
        return {"error": "No email in body"}, 400

    with open("emails.json") as file:
        emails: List = json.load(file)

    if email not in emails:
        return {"error": "You are not subscribed"}, 400

    emails.remove(email)
    with open("emails.json", 'w') as file:
        json.dump(emails, file, indent=4)

    return {"unsubscribe": "success"}, 200


if __name__ == '__main__':
    app.run(debug=True)
