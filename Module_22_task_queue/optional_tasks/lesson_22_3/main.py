import json
import logging
import random

from flask import Flask, request, jsonify
from celery import Celery, group
from celery.app.control import Inspect
import time

app = Flask(__name__)

# Конфигурация Celery
celery = Celery(
    app.name,
    broker='redis://localhost:6379/0',
    backend='redis://localhost:6379/0',
)
inspect = Inspect(app=celery)

loger = logging.getLogger(__name__)
loger.setLevel('DEBUG')


@celery.task
def process_image(image_id: str):
    time.sleep(random.randint(5, 15))
    return f'Image {image_id} processed'


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs) -> None:
   sender.add_periodic_task(10.0, update_info.s(), name='update_info')


@celery.task
def update_info() -> None:
    info = {
        "queries": inspect.active_queues(),
        "tasks": {
            "active": inspect.active(),
            "reserved": inspect.reserved(),
            "scheduled": inspect.scheduled(),
            "revoked": inspect.revoked(),
        },
    }
    with open('info.json', 'w') as file:
        file.write(json.dumps(info))
    loger.debug("Info is updated")


@app.route('/process_images', methods=['POST'])
def process_images():
    images = request.json.get('images')

    if images and isinstance(images, list):
        task_group = group(
            process_image.s(image_id)
            for image_id in images
        )
        result = task_group.apply_async()
        result.save()
        return jsonify({'group_id': result.id}), 202
    else:
        return jsonify({'error': 'Missing or invalid images parameter'}), 400


@app.route('/status/<group_id>', methods=['GET'])
def get_group_status(group_id: str):
    result = celery.GroupResult.restore(group_id)

    if result:
        status = result.completed_count() / len(result)
        return jsonify({'status': status}), 200
    else:
        return jsonify({'error': 'Invalid group_id'}), 404


@app.route('/cancel/<group_id>', methods=['DELETE'])
def cancel_group(group_id: str):
    result = celery.GroupResult.restore(group_id)

    if result:
        celery.control.revoke(group_id)
        return {"canceled": "Ok"}, 200
    else:
        return jsonify({'error': 'Invalid group_id'}), 404


@app.route('/info', methods=['GET'])
def get_info():
    with open('info.json') as file:
        text = file.read()
        data = json.loads(text)
    return jsonify(data), 200


if __name__ == '__main__':
    app.run(debug=True)
