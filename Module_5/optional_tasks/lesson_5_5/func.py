import json
import subprocess, shlex
import time


# bash_path = r'C:\Windows\System32\bash.exe'


def get_ip():
    command = "curl -X GET https://api.ipify.org?format=json"
    with subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
        ip = proc.stdout.read()
    if ip:
        return f'IP: {json.loads(ip).get("ip")}'



def get_ps():
    command = "ps -A"
    # with subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, executable=bash_path) as proc:
    #     processes = proc.stdout.read().decode().split('\n')[1:-1]
    with subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE) as proc:
        processes = proc.stdout.read().decode().split('\n')[1:-1]
    return len(processes)


def newSplit(value):
    lex = shlex.shlex(value)
    lex.quotes = '"'
    lex.whitespace_split = True
    lex.commenters = ''
    return list(lex)


def echo():
    start = time.time()
    command = 'sleep 15'
    procs = []
    for pnum in range(20):
        p = subprocess.Popen(
            newSplit(command),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        print(f'Process number {pnum} started. PID: {p.pid}')
        procs.append(p)

    for pnum, p in enumerate(procs):
        p.wait()
        if p.stdout.read():
            print(f'Process {pnum} successfully finished. PID: {p.pid}')

    print(f'Done in {time.time() - start}')


if __name__ == '__main__':
    print(echo())