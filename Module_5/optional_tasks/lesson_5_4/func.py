import json
import subprocess, shlex


# bash_path = r'C:\Windows\System32\bash.exe'


def get_ip():
    command = "curl -X GET https://api.ipify.org?format=json"
    response = subprocess.run(command, capture_output=True)
    ip = response.stdout.decode()
    if ip:
        return f'IP: {json.loads(ip).get("ip")}'
    return 'Something went wrong'


def get_ps():
    # command = "-c ps -A"
    # response = subprocess.run([bash_path, *shlex.split(command)], capture_output=True)
    command = 'ps -A'
    response = subprocess.run(shlex.split(command), capture_output=True)
    processes = response.stdout.decode().split('\n')[1:-1]
    return len(processes)


if __name__ == '__main__':
    print(get_ps())