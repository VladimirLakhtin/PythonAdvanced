from datetime import datetime
from typing import List, Optional

from flask import Flask, request

app = Flask(__name__)


@app.route('/hi', methods=['GET'])
def test():
    1 / 0
    return 'Ok', 200


if __name__ == '__main__':
    app.run(debug=True)
