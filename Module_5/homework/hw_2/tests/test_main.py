import json
import unittest

from Module_5.homework.hw_2.main import app


class PythonCodeTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['TEST'] = True
        cls.app = app.test_client()
        cls.base_dir = '/'

    def test_can_give_result(self):
        message = 'Hello World!'
        response = self.app.post(self.base_dir, data=dict(
            command=f"print('{message}')",
            timeout=10,
        ))
        self.assertIn(message, response.text)

    def test_cannot_give_result_after_time_out(self):
        message = 'Hello World!'
        response = self.app.post(self.base_dir, data=dict(
            command=f"sleep(2)\nprint('{message}')",
            timout=1
        ))
        self.assertNotIn(message, response.text)

    def test_can_give_out_before_time_out(self):
        message = 'Hello World!'
        response = self.app.post(self.base_dir, data=dict(
            command=f"print('{message}')\nsleep(2)",
            timeout=1,
        ))
        self.assertIn(message, response.text)

    def test_cannot_pass_incorrect_data(self):
        response = self.app.post(self.base_dir, data=dict(
            command="print('Hello World')\nsleep(2)",
            timeout='incorrect data',
        ))
        self.assertEqual(400, response.status_code)

    def test_cannot_pass_additional_command(self):
        message = 'hacked'
        response = self.app.post(self.base_dir, data=dict(
            command=f"print('Hello World')\nsleep(2); echo '{message}'",
            timeout=10,
        ))
        self.assertNotIn(message, response.text)
