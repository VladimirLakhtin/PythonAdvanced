import subprocess

from flask import Flask
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import InputRequired

app = Flask(__name__)


class RunPythonCodeForm(FlaskForm):
    command = StringField(validators=[InputRequired()])
    timeout = IntegerField(validators=[InputRequired()])


@app.route('/', methods=['POST'])
def run_python_code():
    form = RunPythonCodeForm()
    if not form.validate_on_submit():
        return form.errors, 400

    command, timeout = form.command.data, form.timeout.data
    process = subprocess.Popen(
        ['python', '-c', command],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    try:
        outs, errs = process.communicate(timeout=timeout)
    except subprocess.TimeoutExpired:
        process.kill()
        outs, errs = process.communicate()
        errs = b'Time is out\n\n' + errs

    result_message = ''
    if outs:
        result_message += f'Result: {outs.decode()}<br><br>'
    if errs:
        result_message += f'<font color=red>Errors: {errs.decode()}</font>'
    return result_message, 200


if __name__ == '__main__':
    app.config["WTF_CSRF_ENABLED"] = False
    app.run(debug=True)
