import sys
import unittest


from Module_5.homework.hw_4.main import Redirect


class RedirectTestCase(unittest.TestCase):

    def test_can_redirect_only_out(self):
        message = 'Hello'
        stdout_filename = 'stdout.txt'
        stdout = open(stdout_filename, 'w')
        with Redirect(stdout=stdout):
            print(message)

        with open(stdout_filename) as file:
            text = file.read()
            self.assertIn(message, text)

    def test_can_redirect_only_err(self):
        stderr_filename = 'stderr.txt'
        stderr = open(stderr_filename, 'w')
        try:
            with Redirect(stderr=stderr):
                raise Exception
        except Exception as e:
            self.fail(f"Error {e.__class__} is raised: {str(e)}")

        with open(stderr_filename) as file:
            text = file.read()
            self.assertIn('raise Exception', text)

    def test_can_save_old_flows(self):
        message = 'Hello'
        old_stdout = sys.stdout
        old_stderr = sys.stderr
        stdout_filename = 'stdout.txt'
        stderr_filename = 'stdout.txt'
        stdout = open(stdout_filename, 'w')
        stderr = open(stderr_filename, 'w')
        with Redirect(stdout=stdout, stderr=stderr):
            print(message)
            raise Exception

        self.assertEqual(sys.stdout, old_stdout)
        self.assertEqual(sys.stderr, old_stderr)
