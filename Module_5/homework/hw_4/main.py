import sys
import traceback


class Redirect:

    def __init__(self, stdout=None, stderr=None):
        self.new_stdout = stdout
        self.new_stderr = stderr

    def __enter__(self):
        if self.new_stdout:
            self.old_stdout = sys.stdout
            sys.stdout = self.new_stdout

    def __exit__(self, exc_type, exc_val, exc_tb):

        if self.new_stdout:
            self.new_stdout.close()
            sys.stdout = self.old_stdout
        if self.new_stderr:
            if exc_type:
                traceback.print_tb(exc_tb, file=self.new_stderr)
            self.new_stderr.close()
        return True
