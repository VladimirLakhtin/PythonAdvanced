import os
import signal
import subprocess, shlex


def start_server():
    command = 'lsof -i :5000'
    p = subprocess.run(shlex.split(command), capture_output=True)
    out = p.stdout
    if out:
        pids = (int(p.split()[1]) for p in out.decode().split('\n')[1:-1])
        for pid in pids:
            os.kill(pid, signal.SIGKILL)

    subprocess.run(['python3', 'server.py'])


if __name__ == '__main__':
    start_server()