import sys

result = 0
for n in range(1, 11):
    result += n ** 2


with open('self_printing.py') as file:
    sys.stdout.write(file.read())
