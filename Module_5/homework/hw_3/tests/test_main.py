import json
import unittest

from Module_5.homework.hw_2.main import app
from Module_5.homework.hw_3.main import BlockErrors


class BlockErrorsTestCase(unittest.TestCase):

    def test_can_work_without_errors(self):
        try:
            errs_types = {ZeroDivisionError}
            with BlockErrors(errs_types):
                1 / 1
        except TypeError as e:
            self.fail(f"Error {e.__class__} is raised: {str(e)}")


    def test_can_ignore_expected_error(self):
        errs_types = {ZeroDivisionError}
        try:
            with BlockErrors(errs_types):
                1 / 0
        except Exception as e:
            self.fail(f"Error {e.__class__} is raised: {str(e)}")

    def test_can_raise_unexpected_error_out(self):
        errs_types = {ZeroDivisionError}
        with self.assertRaises(TypeError):
            with BlockErrors(errs_types):
                1 / '0'

    def test_can_raise_unexpected_error_inner_and_ignore_expected_one_outer(self):
        error = TypeError
        try:
            outer_errs_types = {error}
            with BlockErrors(outer_errs_types):
                inner_errs_types = {ZeroDivisionError}
                with self.assertRaises(error):
                    with BlockErrors(inner_errs_types):
                        1 / '0'
                raise error
        except error as e:
            self.fail(f"Error {e.__class__} is raised: {str(e)}")

    def test_can_ignore_subclass_errors(self):
        errs_types = {Exception}
        try:
            with BlockErrors(errs_types):
                1 / 0
        except Exception as e:
            self.fail(f"Error {e.__class__} is raised: {str(e)}")
