from typing import Iterable


class BlockErrors:

    def __init__(self, errors: Iterable):
        self.errors = errors

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            for err in self.errors:
                if isinstance(exc_type, err) or issubclass(exc_type, err):
                    return True
