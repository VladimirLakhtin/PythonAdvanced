import http
import json
import re
import typing as t
from wsgiref.simple_server import make_server


class CustomWSGI:
    def __init__(self):
        self.routers: dict[str, t.Callable] = dict()

    @staticmethod
    def _add_pattern_bounds(pattern: str) -> str:
        caret = '' if pattern.startswith('^') else '^'
        dollar = '' if pattern.endswith('$') else '$'
        return rf'{caret}{pattern}{dollar}'

    def route(self, pattern: str) -> t.Callable:
        def wrapper(func: t.Callable) -> t.Callable:
            self.routers[self._add_pattern_bounds(pattern)] = func
            return func

        return wrapper

    @staticmethod
    def parse_path(pattern: str, path: str) -> tuple[str] | None:
        result = re.search(pattern, path, re.I)
        return result and tuple(result.groups())

    def dispatch(self, path: str) -> tuple[int, str | None, dict[str, str]]:
        try:
            for pattern, router in self.routers.items():
                args = self.parse_path(pattern, path)
                if args is not None:
                    status_code, body, headers = router(*args)

                    if isinstance(body, dict):
                        body = json.dumps(body, ensure_ascii=False)
                    else:
                        body = str(body)

                    return status_code, body, headers
            return 404, None, {}
        except Exception:
            return 500, None, {}

    def __call__(self, environ, start_response):
        status_code, body, headers = self.dispatch(environ["REQUEST_URI"])

        http_status_code = http.HTTPStatus(status_code)
        start_response(
            f'{http_status_code.value} {http_status_code.name}',
            [(key, value) for key, value in headers.items()],
        )

        if body is None:
            return [b'']

        if not body.endswith('\n'):
            body += '\n'
        return [body.encode()]
