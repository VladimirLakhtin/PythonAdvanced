from wsgiref.simple_server import make_server

if __name__ == '__main__' and __package__ is None:
    from wsgi import CustomWSGI
else:
    from src.wsgi import CustomWSGI

application = CustomWSGI()


@application.route(r'/hello/(?P<user>\w+)')
def hello_world(user: str):
    return 200, user, {"content-type": "text/plain"}

# with make_server(host='127.0.0.1', port=5000, app=app) as serv:
#     print("OK")
#     serv.handle_request()
