from flask import Flask, request
from flask_restx import Api, Resource
from marshmallow import ValidationError

from Module_17_REST_API.homework.models import (
    get_all_books,
    add_book,
    init_db,
    delete_book_by_id,
    get_book_by_id,
    BOOKS_DATA,
    AUTHORS_DATA, add_author, get_all_author, get_all_book_by_author_id, get_author_by_id, delete_author_by_id,
    update_book,
)
from Module_17_REST_API.homework.schemas import BookSchema, AuthorSchema

app = Flask(__name__)
api = Api(app)


@api.route("/api/books")
class BooksList(Resource):
    def get(self):
        schema = BookSchema()
        return schema.dump(get_all_books(), many=True)

    def post(self):
        data = request.json
        schema = BookSchema()

        try:
            data['author'] = get_author_by_id(data['author_id'])
            if not data['author']:
                return "{'author_id': ['No author with such id']}", 400
            del data['author_id']
            data['author'] = AuthorSchema().dump(data['author'])
            if not data['author']['middle_name']:
                del data['author']['middle_name']
            book = schema.load(data)
        except ValidationError as e:
            return e.messages, 400

        book = add_book(book)
        return schema.dump(book), 201


@api.route("/api/books/<int:id>")
class BookDetails(Resource):
    def get(self, id):
        schema = BookSchema()
        book = get_book_by_id(id)
        if book:
            return schema.dump(book)
        return '', 404

    def put(self, id):
        data = request.json
        schema = BookSchema()

        try:
            data['author'] = get_author_by_id(data['author_id'])
            if not data['author']:
                return "{'author_id': ['No author with such id']}", 400
            del data['author_id']
            data['author'] = AuthorSchema().dump(data['author'])
            if not data['author']['middle_name']:
                del data['author']['middle_name']
            data['id'] = id
            book = schema.load(data)
        except ValidationError as e:
            return e.messages, 400

        book = update_book(book)
        return schema.dump(book), 201

    def delete(self, id):
        if get_book_by_id(id):
            delete_book_by_id(id)
            return '', 200
        return '', 404


@api.route("/api/authors")
class AuthorList(Resource):
    def get(self):
        schema = AuthorSchema()
        return schema.dump(get_all_author(), many=True)

    def post(self):
        data = request.json
        schema = AuthorSchema()

        try:
            author = schema.load(data)
        except ValidationError as e:
            return e.messages, 400

        author = add_author(author)
        return schema.dump(author), 201


@api.route("/api/authors/<int:id>")
class AuthorDetails(Resource):
    def get(self, id: int):
        schema = BookSchema()
        author = get_author_by_id(id)
        if author:
            books = get_all_book_by_author_id(id)
            return schema.dump(books, many=True)
        return '', 404

    def delete(self, id: int):
        if get_author_by_id(id):
            delete_author_by_id(id)
            return '', 200
        return '', 404


if __name__ == '__main__':
    init_db(BOOKS_DATA, AUTHORS_DATA)
    app.config['DEBUG'] = True
    app.run()
