import sqlite3
from dataclasses import dataclass
from typing import List, Dict, Optional

BOOKS_TABLE_NAME = 'books'
AUTHORS_TABLE_NAME = 'authors'
BOOKS_DATA = [
    {
        "title": "A Byte of Python",
        "author_id": 3,
    },
    {
        "title": "Capitan's Daughter",
        "author_id": 2,
    },
    {
        "title": "Tom Sawyer",
        "author_id": 1,
    },
]
AUTHORS_DATA = [
    {
        "first_name": "Mark",
        "last_name": "Twain",
    },
    {
        "first_name": "Alexandr",
        "last_name": "Pushkin",
        "middle_name": "Sergeevich",
    },
    {
        "first_name": "Swaroop",
        "last_name": "C",
        "middle_name": "H",
    },
]


@dataclass(kw_only=True)
class Author:
    id: int | None = None
    first_name: str
    last_name: str
    middle_name: str | None = None


@dataclass(kw_only=True)
class Book:
    id: int | None = None
    title: str
    author: Author


def init_db(books_init_records: List[Dict[str, str]],
            authors_init_records: List[Dict[str, str]]) -> None:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            "SELECT name FROM sqlite_master "
            f"WHERE type='table' AND name='{BOOKS_TABLE_NAME}'"
        )
        exists_books = cursor.fetchone()
        cursor.execute(
            "SELECT name FROM sqlite_master "
            f"WHERE type='table' AND name='{AUTHORS_TABLE_NAME}'"
        )
        exists_authors = cursor.fetchone()
        if not (exists_books and exists_authors):
            cursor.executescript(
                f"CREATE TABLE '{AUTHORS_TABLE_NAME}' ("
                f"id INTEGER PRIMARY KEY AUTOINCREMENT, "
                f"first_name, "
                f"last_name,"
                f"middle_name"
                f")"
            )
            cursor.executescript(
                f"CREATE TABLE '{BOOKS_TABLE_NAME}'("
                f"id INTEGER PRIMARY KEY AUTOINCREMENT, "
                f"title, "
                f"author_id INTEGER NOT NULL, "
                f"FOREIGN KEY (author_id) REFERENCES authors ON DELETE CASCADE"
                f")"
            )
            cursor.executemany(
                f"INSERT INTO '{AUTHORS_TABLE_NAME}' "
                "(first_name, last_name, middle_name) VALUES (?, ?, ?)",
                [
                    (i.get('first_name'), i.get('last_name'), i.get('middle_name'))
                    for i in authors_init_records
                ]
            )
            cursor.executemany(
                f"INSERT INTO '{BOOKS_TABLE_NAME}' "
                "(title, author_id) VALUES (?, ?)",
                [
                    (i['title'], i['author_id'],)
                    for i in books_init_records
                ]
            )


def _get_author_obj_from_row(row) -> Author:
    return Author(id=row[0], first_name=row[1], last_name=row[2], middle_name=row[3])


def _get_book_obj_from_row(row) -> Book:
    return Book(id=row[0], title=row[1], author=get_author_by_id(row[2]))


def get_all_books() -> List[Book]:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            "SELECT * "
            f"FROM '{BOOKS_TABLE_NAME}'"
        )
        all_books = cursor.fetchall()
        return [_get_book_obj_from_row(row) for row in all_books]


def get_all_author() -> List[Author]:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            "SELECT * "
            f"FROM '{AUTHORS_TABLE_NAME}'"
        )
        all_authors = cursor.fetchall()
        return [_get_author_obj_from_row(row) for row in all_authors]


def add_author(author: Author) -> Author:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"INSERT INTO '{AUTHORS_TABLE_NAME}' "
            f"(first_name, last_name, middle_name) VALUES (?, ?, ?)",
            (author.first_name, author.last_name, author.middle_name)
        )
        author.id = cursor.lastrowid
        return author


def add_book(book: Book) -> Book:
    print(book)
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"INSERT INTO '{BOOKS_TABLE_NAME}' "
            f"(title, author_id) VALUES (?, ?)",
            (book.title, book.author.id)
        )
        book.id = cursor.lastrowid
        return book


def get_book_by_id(book_id: int) -> Optional[Book]:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"SELECT id, title, author_id "
            f"FROM '{BOOKS_TABLE_NAME}' "
            f"WHERE id = ?",
            (book_id, )
        )
        book = cursor.fetchone()
        if book:
            return _get_book_obj_from_row(book)


def get_author_by_id(author_id: int) -> Optional[Author]:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"SELECT * "
            f"FROM '{AUTHORS_TABLE_NAME}' "
            f"WHERE id = ?",
            (author_id, )
        )
        author = cursor.fetchone()
        if author:
            return _get_author_obj_from_row(author)


def get_all_book_by_author_id(author_id: int) -> List[Book]:
    with (sqlite3.connect('books.db') as conn):
        cursor = conn.cursor()
        cursor.execute(
            "SELECT * "
            f"FROM '{BOOKS_TABLE_NAME}' "
            f"WHERE author_id = {author_id}"
        )
        all_books = cursor.fetchall()
        return [_get_book_obj_from_row(row) for row in all_books]


def update_book(book: Book) -> Book:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"UPDATE {BOOKS_TABLE_NAME} "
            f"SET title = ?, author_id = ? "
            f"WHERE id = ?",
            (book.title, book.author.id, book.id),
        )
        conn.commit()
        return book


def delete_book_by_id(book_id: int) -> None:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"DELETE "
            f"FROM '{BOOKS_TABLE_NAME}' "
            f"WHERE id = ?",
            (book_id, ),
        )
        conn.commit()


def delete_author_by_id(author_id: int) -> None:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"DELETE "
            f"FROM '{AUTHORS_TABLE_NAME}' "
            f"WHERE id = ?",
            (author_id, ),
        )
        cursor.execute(
            f"DELETE "
            f"FROM '{BOOKS_TABLE_NAME}' "
            f"WHERE author_id = ?",
            (author_id, ),
        )
        conn.commit()


def get_book_by_title(title: str) -> Book:
    with sqlite3.connect('books.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"SELECT id, title, author_id "
            f"FROM '{BOOKS_TABLE_NAME}' "
            f"WHERE title = ?",
            (title, )
        )
        book = cursor.fetchone()
        if book:
            return _get_book_obj_from_row(book)


if __name__ == '__main__':
    init_db(BOOKS_DATA, AUTHORS_DATA)
