import json

from flask import Flask, request

from Module_15_networking.homework.hw_1_TDD.models import get_all_rooms, add_room, reserved_room

app = Flask(__name__)


@app.route('/get-room', methods=['GET'])
def get_rooms():
    return get_all_rooms(), 200


@app.route('/add-room', methods=['POST'])
def add_rooms():
    data = json.loads(request.data.decode())
    add_room(data)
    return get_all_rooms(), 200


@app.route('/booking', methods=['POST'])
def booking():
    data = json.loads(request.data.decode())
    code = reserved_room(data['roomId'])
    response_data = get_all_rooms()
    return response_data, code


if __name__ == '__main__':
    app.config['DEBUG'] = True
    app.run()
