import json
import sqlite3
from typing import List, Dict

DATA = [
    {
        'roomId': 1,
        'floor': 1,
        'guestNum': 2,
        'beds': 1,
        'price': 2500,
        'reserved': False,
    },
    {
        'roomId': 2,
        'floor': 2,
        'guestNum': 3,
        'beds': 2,
        'price': 3500,
        'reserved': False,
    },
    {
        'roomId': 3,
        'floor': 2,
        'guestNum': 2,
        'beds': 1,
        'price': 3000,
        'reserved': False,
    },
]


class Room:
    def __init__(self, roomId: int, floor: int, guestNum: int,
                 beds: int, price: float, reserved: bool = False):
        self.roomId = roomId
        self.floor = floor
        self.guestNum = guestNum
        self.beds = beds
        self.price = price
        self.reserved = reserved

    def __getitem__(self, item):
        return getattr(self, item)

    def __repr__(self):
        return f"{self.__class__.__name__}({self.roomId})"

    def to_json(self):
        return self.__dict__


def init_db(initial_records) -> List[Room]:
    with sqlite3.connect('rooms.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            "SELECT name FROM sqlite_master "
            "WHERE type='table' AND name='table_rooms'"
        )
        exists = cursor.fetchone()
        if not exists:
            cursor.executescript(
                "CREATE TABLE 'table_rooms'"
                "(roomId INTEGER PRIMARY KEY AUTOINCREMENT, floor, guestNum, beds, price, reserved)"
            )
            cursor.executemany(
                "INSERT INTO 'table_rooms' "
                "(floor, guestNum, beds, price, reserved) VALUES (?, ?, ?, ?, ?)",
                [
                    (i['floor'], i['guestNum'], i['beds'], i['price'], i['reserved'])
                    for i in initial_records
                ]
            )


def get_all_rooms() -> str:
    with sqlite3.connect('rooms.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            "SELECT * "
            "FROM 'table_rooms'"
            "WHERE reserved != 1"
        )
        all_rooms = cursor.fetchall()
        return json.dumps([Room(*row).to_json() for row in all_rooms])


def add_room(data: Dict) -> None:
    data['reserved'] = False
    fields = ["floor", "guestNum", "beds", "price", "reserved"]
    with sqlite3.connect('rooms.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"INSERT INTO 'table_rooms' "
            f"({', '.join(fields)}) VALUES (?, ?, ?, ?, ?)",
            [data.get(field) for field in fields]
        )


def reserved_room(room_id) -> int:
    with sqlite3.connect('rooms.db') as conn:
        cursor = conn.cursor()
        cursor.execute(
            f"SELECT * "
            f"FROM 'table_rooms'"
            f"WHERE roomID = {room_id}",
        )
        room = cursor.fetchone()
        if not room:
           return 400
        if room[-1]:
            return 409

        cursor.execute(
            f"UPDATE table_rooms "
            f"SET 'reserved' = 1 "
            f"WHERE roomID = {room_id}",
        )
        return 200


if __name__ == '__main__':
    init_db(DATA)
    print(get_all_rooms())
