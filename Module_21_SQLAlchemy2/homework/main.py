from flask import Flask
from flask_restx import Api

import config
from models import db, create
from routes import std_ns, auth_ns, books_ns

app = Flask(__name__)
app.config.update(
    SECRET_KEY="19916fa6892ff3e3e0e2372223e1a522",
    SQLALCHEMY_DATABASE_URI=config.SQLA_DB_URI,
)
db.init_app(app)
api = Api(
    app,
    version="2.0",
    title="Sample library API",
    description="An API"
)

api.add_namespace(std_ns, path='/api/students')
api.add_namespace(auth_ns, path='/api/authors')
api.add_namespace(books_ns, path='/api/books')

if __name__ == '__main__':
    with app.app_context():
        db.create_all()
        create(db)
    app.run(host='127.0.0.1', port=5000, debug=True)
