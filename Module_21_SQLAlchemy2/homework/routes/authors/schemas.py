from models import Author
from routes.base_schema import BaseSchema


class AuthorSchema(BaseSchema):

    class Meta:
        model = Author
