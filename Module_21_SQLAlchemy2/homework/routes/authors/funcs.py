from models import db, Author


def get_author_by_id(author_id: int):
    return db.session.query(Author).filter(Author.id == author_id).one()


def get_count_available_books(author_id: int) -> int:
    author = get_author_by_id(author_id)
    return sum(1 for book in author.books if book.count > 0)
