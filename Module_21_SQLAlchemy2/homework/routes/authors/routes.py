from flask_restx import Namespace, Resource
from sqlalchemy.exc import NoResultFound

from .funcs import get_count_available_books
ns = Namespace('authors', description='Authors related operations')


@ns.route('/<int:author_id>/available', methods=['GET'])
class CountAvailableBooksByAuthorResource(Resource):
    def get(self, author_id: int):
        try:
            available_books_count = get_count_available_books(author_id)
        except NoResultFound:
            return {"error": "No author with such id"}, 404
        return {
            'author_id': author_id,
            'available_books_count': available_books_count,
        }
