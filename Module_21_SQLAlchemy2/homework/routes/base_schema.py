from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

from models import db


class BaseSchema(SQLAlchemyAutoSchema):
    sqla_session = db.session

    class Meta:
        sqla_session = db.session