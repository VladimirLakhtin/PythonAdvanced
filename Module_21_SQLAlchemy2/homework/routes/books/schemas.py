from typing import Dict

from marshmallow import validates_schema, post_load, ValidationError, fields

from .funcs import issue_book, return_book
from models import ReceivingBooks, Book, Student
from ..authors.schemas import AuthorSchema
from ..base_schema import BaseSchema


class BookSchema(BaseSchema):

    class Meta:
        model = Book

    author = fields.Nested(AuthorSchema)


class BaseReceivingBooksSchema(BaseSchema):

    class Meta(BaseSchema.Meta):
        model = ReceivingBooks
        include_fk = True
        load_instance = True

    @validates_schema
    def validate_object(self, data: Dict, **kwargs) -> None:

        self.book = self.sqla_session.query(Book) \
            .filter(Book.id == data["book_id"]) \
            .first()

        if not self.book:
            raise ValidationError("No book with this id")

        student = self.sqla_session.query(Student) \
            .filter(Student.id == data["student_id"]) \
            .first()

        if not student:
            raise ValidationError("No student with this id")


class IssueReceivingBooksSchema(BaseReceivingBooksSchema):

    @validates_schema
    def validate_object(self, data: Dict, **kwargs) -> None:

        super().validate_object(data)

        if self.sqla_session.query(ReceivingBooks) \
                .filter(ReceivingBooks.book_id == data["book_id"]) \
                .filter(ReceivingBooks.student_id == data["student_id"]) \
                .filter(ReceivingBooks.date_of_return.is_(None)) \
                .first():
            raise ValidationError("The Student already reviewed this Book")

        if self.book.count == 0:
            raise ValidationError("This book is over")

    @post_load
    def issue_book(self, data: Dict, **kwargs) -> Dict:
        issue_book(**data)
        return data


class ReturnReceivingBooksSchema(BaseReceivingBooksSchema):

    @validates_schema
    def validate_object(self, data: Dict, **kwargs) -> None:

        super().validate_object(data)

        if not self.sqla_session.query(ReceivingBooks) \
                .filter(ReceivingBooks.book_id == data["book_id"]) \
                .filter(ReceivingBooks.student_id == data["student_id"]) \
                .filter(ReceivingBooks.date_of_return.is_(None)) \
                .first():
            raise ValidationError("The Student didn't review this Book")

    @post_load
    def c_return_book(self, data: Dict, **kwargs) -> Dict:
        return_book(**data)
        return data
