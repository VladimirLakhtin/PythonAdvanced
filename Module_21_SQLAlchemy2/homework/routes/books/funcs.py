import datetime
from typing import List

from models import db, Book, ReceivingBooks, Student
from sqlalchemy import update, func, extract


def get_all_books() -> List[Book]:
    return db.session.query(Book).all()


def issue_book(book_id: int, student_id: int) -> None:
    db.session.add(ReceivingBooks(book_id=book_id, student_id=student_id))
    book = db.session.query(Book).filter(Book.id == book_id).one()
    book.count -= 1
    db.session.commit()


def return_book(book_id: int, student_id: int) -> None:
    query = update(ReceivingBooks) \
        .where(ReceivingBooks.book_id == book_id) \
        .where(ReceivingBooks.student_id == student_id) \
        .values(date_of_return=datetime.datetime.now())
    db.session.execute(query)
    book = db.session.query(Book).filter(Book.id == book_id).one()
    book.count += 1
    db.session.commit()


def get_recommendations(student_id) -> List[Book]:
    student: Student = db.session.query(Student).filter(Student.id == student_id).one()
    books_already_read: set[Book] = student.books
    result = []
    for book in books_already_read:
        author_books = book.author.books
        recommended_author_books = author_books.copy()
        recommended_author_books.remove(book)
        result.extend(recommended_author_books)
    return result


def get_avg_of_books_issued() -> float:
    current_month = datetime.datetime.now().month
    issued_books_by_students = db.session.query(func.count(ReceivingBooks.book_id)) \
        .filter(extract('month', ReceivingBooks.date_of_issue) == current_month) \
        .group_by(ReceivingBooks.student_id) \
        .all()
    result = sum(rec[0] for rec in issued_books_by_students) / len(issued_books_by_students)
    return round(result, 2)


def get_popular_book() -> Book:
    books_popularity = db.session.query(func.count(ReceivingBooks.student_id), Book) \
        .join(Student) \
        .join(Book) \
        .filter(Student.average_score > 4) \
        .group_by(ReceivingBooks.book_id) \
        .all()
    return max(books_popularity, key=lambda x: x[0])[1]
