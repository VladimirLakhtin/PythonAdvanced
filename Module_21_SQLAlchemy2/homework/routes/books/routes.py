from flask import request
from flask_restx import Resource, Namespace
from marshmallow import ValidationError
from sqlalchemy.exc import NoResultFound

from .funcs import get_all_books, get_recommendations, get_avg_of_books_issued, get_popular_book
from .schemas import IssueReceivingBooksSchema, ReturnReceivingBooksSchema, BookSchema

ns = Namespace('books', description='Books related operations')


@ns.route('', methods=['GET'])
class BooksResource(Resource):
    def get(self):
        schema = BookSchema()
        books = get_all_books()
        books_data = schema.dump(obj=books, many=True)
        return {"books": books_data}, 200


@ns.route('/issue', methods=['POST'])
class IssueBookResource(Resource):
    def post(self):
        schema = IssueReceivingBooksSchema()
        data = request.json
        try:
            schema.load(data)
        except ValidationError as e:
            return e.messages_dict, 400
        return 'Ok', 200


@ns.route('/return', methods=['POST'])
class ReturnBookResource(Resource):
    def post(self):
        schema = ReturnReceivingBooksSchema()
        data = request.json
        try:
            schema.load(data)
        except ValidationError as e:
            return e.messages_dict, 400
        return 'Ok', 200


@ns.route('/recommendations/<int:student_id>', methods=['GET'])
class RecommendationsResource(Resource):
    def get(self, student_id: int):
        schema = BookSchema()
        try:
            books = get_recommendations(student_id)
        except NoResultFound:
            return {"error": "No student with such id"}, 404
        books_data = schema.dump(books, many=True)
        return {"books": books_data}


@ns.route('/avg-issued')
class AvgBooksIssuedResource(Resource):
    def get(self):
        data = get_avg_of_books_issued()
        return {"avg": data}


@ns.route('/popular')
class PopularBookResource(Resource):
    def get(self):
        schema = BookSchema()
        book = get_popular_book()
        book_data = schema.dump(book)
        return {"popular": book_data}
