import datetime
from typing import List, Dict

from models import db, Student, ReceivingBooks
from sqlalchemy import func, extract


def get_debtors_count() -> int:
    expiration_date = datetime.datetime.now() - datetime.timedelta(days=14)
    return db.session \
        .query(func.count(func.distinct(ReceivingBooks.student_id))) \
        .filter(ReceivingBooks.date_of_return.is_(None)) \
        .filter(ReceivingBooks.date_of_issue < expiration_date) \
        .scalar()


def get_top_10_most_reading() -> List[Student]:
    current_year = datetime.datetime.now().year
    return db.session.query(Student) \
        .join(ReceivingBooks) \
        .filter(extract("year", ReceivingBooks.date_of_issue) == current_year) \
        .group_by(ReceivingBooks.student_id) \
        .order_by(-func.count(ReceivingBooks.book_id)) \
        .limit(10) \
        .all()


def create_students_csv(dict_reader: List[Dict]) -> None:
    print(dict_reader)
