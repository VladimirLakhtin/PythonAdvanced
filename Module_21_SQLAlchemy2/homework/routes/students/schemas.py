from models import Student
from routes.base_schema import BaseSchema


class StudentSchema(BaseSchema):

    class Meta:
        model = Student
