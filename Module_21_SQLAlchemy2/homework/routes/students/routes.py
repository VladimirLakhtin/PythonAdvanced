import csv

from flask import request
from flask_restx import Namespace, Resource

from .funcs import get_debtors_count, get_top_10_most_reading, create_students_csv
from .schemas import StudentSchema

ns = Namespace('students', description='Students related operations')


@ns.route('/debtors', methods=['GET'])
class CountDebtorsResource(Resource):
    def get(self):
        return {'count_debtors': get_debtors_count()}


@ns.route('/rating', methods=['GET'])
class RatingResource(Resource):
    def get(self):
        schema = StudentSchema()
        rating = get_top_10_most_reading()
        rating_data = schema.dump(rating, many=True)
        return {"rating": rating_data}


@ns.route('/create-csv', methods=['POST'])
class CreateCSVResource(Resource):
    def post(self):
        print('request.method', request.method)
        print('request.args', request.args)
        print('request.form', request.form)
        print('request.files', request.files)

        try:
            dict_reader = csv.reader(request.data)
        except TypeError:
            return {'error': 'TypeError: data should be is csv file'}, 400
        create_students_csv(dict_reader)
        return {}
