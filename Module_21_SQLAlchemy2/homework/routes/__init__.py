from .students import ns as std_ns
from .authors import ns as auth_ns
from .books import ns as books_ns
