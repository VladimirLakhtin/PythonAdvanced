import datetime
from typing import Dict, Any

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .database import db


class Book(db.Model):
    __tablename__ = "book"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    count: Mapped[int] = mapped_column(default=1)
    realize_date: Mapped[datetime.date]
    author_id: Mapped[int] = mapped_column(ForeignKey('author.id'))

    author: Mapped["Author"] = relationship(back_populates="books")

    def to_json(self) -> Dict[str, Any]:
        book_data = {c.name: getattr(self, c.name)
                     for c in self.__table__.columns
                     if c.name != 'realize_date'}
        book_data["realize_date"] = str(self.realize_date)
        return book_data

    @classmethod
    def get_all_books(cls):
        return db.session.query(Book).all()