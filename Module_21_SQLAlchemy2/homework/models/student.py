import datetime
from typing import List, Set

from sqlalchemy import ForeignKey, func, update
from sqlalchemy.ext.associationproxy import AssociationProxy, association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .database import db


class Student(db.Model):
    __tablename__ = "student"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    surname: Mapped[str]
    phone: Mapped[str]
    email: Mapped[str]
    average_score: Mapped[float]
    scholarship: Mapped[bool]

    receiving_books: Mapped[List["ReceivingBooks"]] = relationship(
        back_populates="student")

    books: AssociationProxy[Set["Book"]] = association_proxy(
        "receiving_books",
        "book",
        creator=lambda book: ReceivingBooks(book=book),
    )

    # @classmethod
    # def get_students_with_scholarship(cls) -> List:
    #     return db.session.query(cls).filter(cls.scholarship).all()
    #
    # @classmethod
    # def get_students_with_score_above(cls, score: float) -> List:
    #     return db.session.query(cls).filter(cls.average_score > score).all()


class ReceivingBooks(db.Model):
    __tablename__ = "receiving_books"

    id: Mapped[int] = mapped_column(primary_key=True)
    book_id: Mapped[int] = mapped_column(ForeignKey("book.id"))
    student_id: Mapped[int] = mapped_column(ForeignKey("student.id"))
    date_of_issue: Mapped[datetime.datetime] = mapped_column(
        default=datetime.datetime.now())
    date_of_return: Mapped[datetime.datetime | None]

    student: Mapped[Student] = relationship(back_populates="receiving_books")
    book: Mapped["Book"] = relationship()

    @hybrid_property
    def count_date_with_book(self) -> int:
        date_end = self.date_of_return or datetime.datetime.now()
        return (date_end - self.date_of_issue).day


