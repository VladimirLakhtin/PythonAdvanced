from typing import List

from sqlalchemy.orm import Mapped, mapped_column, relationship

from .database import db


class Author(db.Model):
    __tablename__ = "author"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    surname: Mapped[str]

    books: Mapped[List["Book"]] = relationship(
        back_populates="author",
        cascade="all",
    )
