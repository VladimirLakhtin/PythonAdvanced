import datetime

from .author import Author
from .student import Student
from .book import Book


BOOKS_DATA = [
    {
        "name": "A Byte of Python",
        "realize_date": datetime.datetime.now(),
        "count": 2,
        "author_id": 3,
    },
    {
        "name": "Capitan's Daughter",
        "realize_date": datetime.datetime.now(),
        "count": 5,
        "author_id": 2,
    },
    {
        "name": "Tom Sawyer",
        "realize_date": datetime.datetime.now(),
        "count": 3,
        "author_id": 1,
    },
]

AUTHORS_DATA = [
    {
        "name": "Mark",
        "surname": "Twain",
    },
    {
        "name": "Alexandr",
        "surname": "Pushkin",
    },
    {
        "name": "Swaroop",
        "surname": "C",
    },
]

STUDENTS_DATA = [
    {
        "name": "Vladik",
        "surname": "Pupkin",
        "phone": "+79999999999",
        "email": "p@gmail.com",
        "average_score": 4.5,
        "scholarship": True,
    },
    {
        "name": "Vovchik",
        "surname": "Petrov",
        "phone": "+79998888888",
        "email": "v@gmail.com",
        "average_score": 3.8,
        "scholarship": False,
    },
    {
        "name": "Alina",
        "surname": "Ivanova",
        "phone": "+79997777777",
        "email": "a@gmail.com",
        "average_score": 4.9,
        "scholarship": True,
    },
]


def create(db):
    if not db.session.query(Author).all():
        db.session.bulk_insert_mappings(Author, AUTHORS_DATA)

    if not db.session.query(Book).all():
        db.session.bulk_insert_mappings(Book, BOOKS_DATA)

    if not db.session.query(Student).all():
        db.session.bulk_insert_mappings(Student, STUDENTS_DATA)

    db.session.commit()
