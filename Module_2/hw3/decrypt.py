import sys


def decrypt(encryption: str) -> str:
    result = ''
    flag = False
    for i in encryption:
        if i != '.':
            result += i
            flag = False
        elif flag:
            if result:
                result = result[:-1]
            flag = False
        else:
            flag = True
    return result


if __name__ == '__main__':
    data = sys.stdin.read()
    message = decrypt(data)
    print(message)
