"""
Реализуйте endpoint /hello-world/<имя>, который возвращает строку «Привет, <имя>. Хорошей пятницы!».
Вместо хорошей пятницы endpoint должен уметь желать хорошего дня недели в целом, на русском языке.

Пример запроса, сделанного в субботу:

/hello-world/Саша  →  Привет, Саша. Хорошей субботы!
"""
from datetime import datetime

from flask import Flask


weekdays = ('Понедельника', "Вторника", "Среды", "Четверга", "Пятницы", "Субботы", "Воскресенья")

app = Flask(__name__)


@app.route('/hello-world/<string:name>')
def index(name: str) -> str:
    weekday_id = datetime.today().weekday()
    weekday = weekdays[weekday_id]
    ending = 'его' if weekday_id in (0, 1, 3, 6) else 'ей'
    return f'Привет, {name}. Хорош{ending} {weekday}'


if __name__ == '__main__':
    app.run(debug=True)
