"""
Реализуйте приложение для учёта финансов, умеющее запоминать, сколько денег было потрачено за день,
а также показывать затраты за отдельный месяц и за целый год.

В программе должно быть три endpoints:

/add/<date>/<int:number> — сохранение информации о совершённой в рублях трате за какой-то день;
/calculate/<int:year> — получение суммарных трат за указанный год;
/calculate/<int:year>/<int:month> — получение суммарных трат за указанные год и месяц.

Дата для /add/ передаётся в формате YYYYMMDD, где YYYY — год, MM — месяц (от 1 до 12), DD — число (от 01 до 31).
Гарантируется, что переданная дата имеет такой формат и она корректна (никаких 31 февраля).
"""


from flask import Flask


app = Flask(__name__)
data = {}


def summary(year: str, month: str = None) -> int:
    try:
        year_dict = data[year]
        if month:
            month_dict = year_dict[month]
            spends_sum = sum(month_dict.values())
        else:
            spends_sum = sum([
                sum(month.values())
                for month in year_dict.values()
            ])
        return spends_sum
    except KeyError:
        raise ValueError('No spends in this date')


@app.route('/add/<string:date>/<int:number>')
def add_spend(date: str, number: int):
    year = int(date[:4])
    month = int(date[4:6])
    day = int(date[6:])

    year_dict = data.setdefault(year, {})
    month_dict = year_dict.setdefault(month, {})
    month_dict.setdefault(day, 0)
    month_dict[day] += number
    print(data)
    return 'Запись сохранено'


@app.route('/calculate/<int:year>')
def calculate_by_year(year):
    try:
        spends_sum = summary(year)
        return f'Spending for {year}: {spends_sum}'
    except ValueError as e:
        return str(e)


@app.route('/calculate/<int:year>/<int:month>')
def calculate_by_year_month(year, month):
    try:
        spends_sum = summary(year, month)
        return f'Spending for {month}.{year}: {spends_sum}'
    except ValueError as e:
        return str(e)


if __name__ == "__main__":
    app.run(debug=True)
