import os

from flask import Flask


app = Flask(__name__)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


@app.route('/preview/<int:size>/<path:path>')
def file_preview(size: int, path: str):
    rel_path = os.path.join(BASE_DIR, path)
    if not os.path.exists(rel_path):
        return "File doesn't exists"
    abs_path = os.path.abspath(rel_path)
    with open(rel_path, 'r', encoding='utf-8') as file:
        preview = file.read(size)
    return (f"{abs_path} {len(preview)}<br>"
            f"{preview}")


if __name__ == '__main__':
    app.run(debug=True)
