import sys
from typing import List


def get_mean_size(lines: List) -> float:
    summary = sum([int(row.split()[4]) for row in lines])
    mean = summary / len(lines)
    return mean


if __name__ == '__main__':
    ls_output = sys.stdin.readlines()[1:]
    mean_size = get_mean_size(ls_output)
    print(mean_size)

