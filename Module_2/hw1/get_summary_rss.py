import math
from typing import List


def get_summary_rss(path: str) -> int:
    with open(path) as file:
        lines = file.readlines()[1:]
    result = sum([int(row.split()[5]) for row in lines])
    return result


def print_result(size: int) -> None:
    pwr = math.floor(math.log(size, 1024))
    suff = ["Б", "КБ", "МБ", "ГБ", "ТБ", "ПБ", "ЭБ", "ЗБ", "ЙБ"]
    if size > 1024 ** (len(suff) - 1):
        print("Too much :)")
    print(f"{size / 1024 ** pwr:.0f}{suff[pwr]}")


if __name__ == '__main__':
    rss_path = 'output.txt'
    summary_rss = get_summary_rss(rss_path)
    print_result(summary_rss)
