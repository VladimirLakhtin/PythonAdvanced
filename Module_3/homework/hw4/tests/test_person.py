import unittest

from freezegun import freeze_time

from Module_3.homework.hw4.person import Person


class PersonTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.name = 'Ivan'
        cls.yob = 2002
        cls.address = 'Pupkina st. 21'

    def setUp(self) -> None:
        self.person = Person(self.name, self.yob, self.address)

    @freeze_time('2023-08-24')
    def test_can_get_age(self):
        target_age = 21
        age = self.person.get_age()
        self.assertEqual(target_age, age)

    def test_can_get_name(self):
        target_name = self.name
        name = self.person.get_name()
        self.assertEqual(target_name, name)

    def test_can_get_address(self):
        target_address = self.address
        address = self.person.get_address()
        self.assertEqual(target_address, address)

    def test_can_set_name(self):
        target_name = 'Jack'
        self.person.set_name(target_name)
        name = self.person.get_name()
        self.assertEqual(target_name, name)

    def test_can_set_address(self):
        target_address = 'Kolotushkina st. 52'
        self.person.set_address(target_address)
        address = self.person.get_address()
        self.assertEqual(target_address, address)

    def test_can_get_home_status(self):
        is_homeless = self.person.is_homeless()
        self.assertFalse(is_homeless)

        self.person = Person(self.name, self.yob)
        self.assertTrue(self.person.is_homeless())

