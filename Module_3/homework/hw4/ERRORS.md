## Errors in person code:
### 1. Age always returned negative. Switch of subtrahend and minuend in years difference. Correct code:
```python
def get_age(self) -> int:
    now: datetime.datetime = datetime.datetime.now()
    return now.year - self.year
```
### 2. Name is not set. Use self.name instead name. Correct code:
```python
def set_name(self, name: str) -> None:
    self.name = name
```
### 3. Address is not set. Two equals instead of one. Correct code:
```python
def set_address(self, address: str) -> None:
    self.address = address
```
### 4. Always returns False. 
#### * Use address instead self.address. Correct method code:
```python
def is_homeless(self) -> bool:
    return self.address is None
``` 
#### * Use '' as default value fo address instead None. Correct __init__ code:
```python
def __init__(self, name: str, year_of_birth: int, address: str = None) -> None:
    self.name: str = name
    self.yob: int = year_of_birth
    self.address: str = address
```