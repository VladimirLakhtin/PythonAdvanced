import unittest

from freezegun import freeze_time
from Module_3.homework.hw1.hello_word_with_day import app


class TestHelloWorldWithDay(unittest.TestCase):

    def setUp(self) -> None:
        app.config['TEST'] = True
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.base_dir = '/hello-world/'

    def test_contain_name(self) -> None:
        name = 'username'
        response = self.app.get(self.base_dir + name)
        response_text = response.data.decode()
        self.assertIn(name, response_text)

    def test_cannot_pass_name_with_slash(self) -> None:
        name = 'user/name'
        response = self.app.get(self.base_dir + name)
        self.assertEqual(response.status, '404 NOT FOUND')

    @freeze_time("2023-08-24")
    def test_contain_correct_weekday(self) -> None:
        name = 'username'
        weekday_text = 'Хорошего четверга'
        self.assertNotIn(weekday_text, name)
        response = self.app.get(self.base_dir + name)
        response_text = response.data.decode()
        self.assertIn(weekday_text, response_text)
