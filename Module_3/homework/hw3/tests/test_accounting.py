import unittest

from Module_3.homework.hw3 import accounting
from Module_3.homework.hw3.tests.test_data import test_data


class AccountingTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        accounting.app.config['DEBUG'] = False
        accounting.app.config['TEST'] = True
        cls.app = accounting.app.test_client()
        cls.base_dir = '/'

        accounting.data = test_data

    def test_can_add_not_exist_date(self):
        date = '20240824'
        value = '200'
        year, month, day = 2024, 8, 24
        self.app.get(self.base_dir + 'add/' + '/'.join((date, value)))

        self.assertIn(year, accounting.data)
        self.assertIsInstance(accounting.data[year], dict)
        self.assertIn(month, accounting.data[year])
        self.assertIsInstance(accounting.data[year][month], dict)
        self.assertIn(day, accounting.data[year][month])
        self.assertIsInstance(accounting.data[year][month][day], int)
        self.assertEqual(int(value), accounting.data[year][month][day])

    def test_can_add_exist_date(self):
        date = '20230824'
        value = '200'
        year, month, day = 2023, 8, 24
        previous_value = accounting.data[year][month][day]
        target_value = previous_value + int(value)
        self.app.get(self.base_dir + 'add/' + '/'.join((date, value)))

        result_value = accounting.data[year][month][day]
        self.assertEqual(target_value, result_value)

    def test_add_cannot_pass_incorrect_date(self):
        dates = '202308244', '2023a824', '2023082'
        value = '100'
        for date in dates:
            with self.subTest(date=date):
                response = self.app.get('add/' + '/'.join((date, value)))
                self.assertIn('422', response.status)

    def test_calculate_year(self):
        year = '2023'
        target_value = '1200'
        response = self.app.get(self.base_dir + 'calculate/' + year)
        response_text = response.data.decode()
        self.assertIn(target_value, response_text)

    def test_calculate_year_month(self):
        year, month = '2023', '08'
        target_value = '600'
        response = self.app.get(self.base_dir + 'calculate/' + '/'.join((year, month)))
        response_text = response.data.decode()
        self.assertIn(target_value, response_text)

    def test_calculate_with_no_data(self):
        year, month = '2024', '08'
        response = self.app.get(self.base_dir + 'calculate/' + '/'.join((year, month)))
        response_text = response.data.decode().replace(' ', '')
        self.assertIn('200', response.status)
        self.assertTrue(response_text.isalpha())