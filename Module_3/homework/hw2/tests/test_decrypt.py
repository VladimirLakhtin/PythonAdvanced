import unittest

from Module_3.homework.hw2.decrypt import decrypt


class DecryptTestCase(unittest.TestCase):

    def test_crypt_with_no_dots(self):
        request = 'абра-кадабра'
        response = decrypt(request)
        self.assertEqual(request, response)

    def test_crypt_with_one_dot(self):
        request = 'абра-кадабра.'
        target_response = 'абра-кадабра'
        response = decrypt(request)
        self.assertEqual(target_response, response)

    def test_crypt_with_two_dots(self):
        requests = 'абраа..-кадабра', 'абра--..кадабра'
        target_response = 'абра-кадабра'
        for request in requests:
            with self.subTest(request=request):
                response = decrypt(request)
                self.assertEqual(response, target_response)

    def test_crypt_with_three_dots(self):
        requests = 'абраа..-.кадабра', 'абрау...-кадабра', '1..2.3'
        target_responses = 'абра-кадабра', 'абра-кадабра', '23'
        for request, target_response in zip(requests, target_responses):
            with self.subTest(request=request):
                response = decrypt(request)
                self.assertEqual(response, target_response)

    def test_crypt_with_many_dots(self):
        requests = '1.......................', 'абр......a.', 'абра........'
        target_responses = '', 'a', ''
        for request, target_response in zip(requests, target_responses):
            with self.subTest(request=request):
                response = decrypt(request)
                self.assertEqual(response, target_response)

    def test_crypt_is_one_dot(self):
        request = '.'
        target_response = ''
        response = decrypt(request)
        self.assertEqual(target_response, response)
