import unittest

from max_number import app


class TestMaxNumber(unittest.TestCase):

    def setUp(self) -> None:
        app.config['TEST'] = True
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.base_dir = '/max_number/'

    def test_can_get_max_number(self):
        numbers = 13, 54, 12
        response = self.app.get(self.base_dir + '/'.join(str(n) for n in numbers))
        response_text = response.data.decode()
        expected_number = str(max(numbers))
        self.assertIn(expected_number, response_text)
