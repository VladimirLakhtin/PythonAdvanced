import unittest

from Module_3.optional_tasks.hello_word_with_name import app


class TestHelloWorldWithName(unittest.TestCase):

    def setUp(self) -> None:
        app.config['TEST'] = True
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.base_dir = '/hello-world/'

    def test_contain_name(self):
        name = 'username'
        response = self.app.get(self.base_dir + name)
        response_text = response.data.decode()
        self.assertIn(name, response_text)

    def test_cannot_pass_name_with_slash(self):
        name = 'user/name'
        response = self.app.get(self.base_dir + name)
        self.assertEqual(response.status, '404 NOT FOUND')
