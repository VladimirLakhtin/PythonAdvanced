def get_social_status(age: int) -> str:
    if type(age) not in (float, int):
        raise ValueError('Age should be a number')
    elif age < 0:
        raise ValueError('Age cannot be negative number')
    elif 0 <= age < 13:
        return 'child'
    elif 13 <= age < 18:
        return 'teenager'
    elif 18 <= age < 50:
        return 'adult'
    elif 50 <= age < 65:
        return 'elderly'
    else:
        return 'pensioner'
