from flask import Flask


app = Flask(__name__)


@app.route('/max_number/<path:numbers>')
def get_max_number(numbers: str) -> str:
    max_number = max(numbers.split('/'))
    return f'Максимальное число: {max_number}'


if __name__ == '__main__':
    app.run(debug=True)
