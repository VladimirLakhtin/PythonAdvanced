import logging
import math
import multiprocessing
import time
from multiprocessing.pool import ThreadPool

logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(message)s")
logger = logging.getLogger(__name__)


def factorial_synchronously():
    start = time.time()
    factorials = sum(
        math.factorial(i)
        for i in range(1, 10000 + 1)
    )
    logger.info(f"[Synchronously] Time taken in seconds - {time.time() - start}")


def factorial_multithreading():
    input_values = list(range(1, 10000 + 1))
    start = time.time()
    with ThreadPool(processes=multiprocessing.cpu_count() * 5) as pool:
        result = pool.map(math.factorial, input_values)
    logger.info(f"[Multithreading] Time taken in seconds - {time.time() - start}")


def factorial_multiprocessing():
    input_values = list(range(1, 10000 + 1))
    start = time.time()
    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        result = pool.map(math.factorial, input_values)
    logger.info(f"[Multiprocessing] Time taken in seconds - {time.time() - start}")


def main():
    factorial_synchronously()
    factorial_multithreading()
    factorial_multiprocessing()


if __name__ == '__main__':
    main()