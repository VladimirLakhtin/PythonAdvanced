import os
import shlex
import subprocess


def process_count(username: str) -> int:
    ps_process = subprocess.Popen(['ps', '-u', username],
                                  stdout=subprocess.PIPE)
    wc_process = subprocess.Popen(['wc', '-l'], stdin=ps_process.stdout,
                                  stdout=subprocess.PIPE)
    out, err = wc_process.communicate()
    return int(out.decode()[:-1]) - 3


def total_memory_usage(root_pid: int) -> float:
    ps_process = subprocess.Popen(['ps', '-u', '--ppid', str(root_pid)],
                                  stdout=subprocess.PIPE)
    awk_command = "awk 'BEGIN {sum=0} {sum +=$6} END {print sum}'"
    awk_process = subprocess.Popen(shlex.split(awk_command),
                                  stdin=ps_process.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = awk_process.communicate()
    return float(out.decode()[:-1])


if __name__ == '__main__':
    username = os.path.expanduser('~').split('/')[-1]
    print(process_count(username))
    print(total_memory_usage(os.getgid()))
