from datetime import datetime
from typing import List, Optional

from flask import Flask, request

app = Flask(__name__)


def check_data(
        cell_tower_ids: List[int],
        phone_prefixes: List[str],
        protocols: List[str],
        signal_level: Optional[float],
        date_from: str,
        date_to: str,
) -> None:
    for tower_id in cell_tower_ids:
        if tower_id <= 0 or not isinstance(tower_id, int):
            raise ValueError('Tower id must be a positive number')

    for phone_prefix in phone_prefixes:
        if not isinstance(phone_prefix, str):
            raise ValueError('Phone prefix must be a string type')
        if not phone_prefix.endswith('*'):
            raise ValueError('Phone prefix must ends with "*"')
        if len(phone_prefix) > 11:
            raise ValueError('Phone prefix must not contain more than 11 digits')

    for protocol in protocols:
        if protocol not in ('2G', '3G', '4G'):
            raise ValueError('Allowed protocol values: 2G, 3G, 4G')

    for date in date_from, date_to:
        datetime_date: datetime = datetime.strptime(date, "%Y%m%d")
        if len(date) != 8:
            raise ValueError('Date must be in the YYYYMMDD format')
        if not date.isdigit():
            raise ValueError('Date must consist of digits')
        if datetime_date > datetime.now():
            raise ValueError('It is not possible to make a request for a future date')
    if date_to < date_from:
        raise ValueError('date_from must be less than date_to')


@app.route('/search/', methods=['GET'])
def search():
    cell_tower_ids: List[int] = request.args.getlist('tower_id', type=int)

    if not cell_tower_ids:
        return 'You must specify at least one cell_tower_id', 400

    phone_prefixes: List[str] = request.args.getlist('phone_prefix')
    protocols: List[str] = request.args.getlist('protocol')
    signal_level: Optional[float] = request.args.get('signal_level', type=float, default=None)
    date_from: str = request.args.get('date_from', default=None)
    date_to: str = request.args.get('date_to', default=None)

    try:
        check_data(cell_tower_ids, phone_prefixes, protocols, signal_level, date_from, date_to)
    except ValueError as e:
        return str(e), 400

    return (
        f"Search criteries:"
        f"tower_ids: {cell_tower_ids} call towers. "
        f"phone_prefixes: {phone_prefixes}; "
        f"protocols: {protocols}; "
        f"signal_level: {signal_level}; "
        f"date_from: {date_from}; "
        f"date_to: {date_to}; "
    )


@app.route('/mul-sum/<int:a>/<int:b>')
def mul_and_sum(a: int, b: int):
    mul = a * b
    summ = a + b
    return (f"Sum: {summ}"
            f"Mul: {mul}")


if __name__ == '__main__':
    app.run(debug=True)
