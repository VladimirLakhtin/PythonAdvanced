import re
import unittest
from typing import Dict, List

from Module_4.optional_tasks.lesson_4_2.main import app


class SearchCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        app.config['DEBUG'] = False
        app.config['TEST'] = True
        cls.app = app.test_client()
        cls.data = {
            'tower_id': [1, 2, 3],
            'phone_prefix': ['999*', '1234567890*', '9*'],
            'protocol': ['2G', '3G', '4G'],
            'signal_level': 17.05,
            'date_from': '20230824',
            'date_to': '20230825',
        }

    @staticmethod
    def get_search_path(data: Dict) -> str:
        path = '?'
        for key, value in data.items():
            if isinstance(value, List):
                for item in value:
                    path += f"{key}={item}&"
            else:
                path += f"{key}={value}&"
        return path

    def test_can_get_right_data(self):
        path = self.get_search_path(self.data)
        response = self.app.get('/search/' + path)
        response_text = response.data.decode()
        with self.subTest(status=response.status):
            self.assertIn('200', response.status)
        for key, value in self.data.items():
            with self.subTest(key=key, value=value):
                results = re.findall(rf'{key}.*{value}', response_text)
                self.assertTrue(len(results) == 1)

    def test_can_get_mul_and_sum(self):
        num_1 = 9
        num_2 = 2
        response = self.app.get(f'/mul-sum/{num_1}/{num_2}')
        response_text = response.data.decode()
        self.assertIn(str(num_1 * num_2), response_text)
        self.assertIn(str(num_1 + num_2), response_text)

    def test_cannot_pass_not_number(self):
        num_1 = 'a'
        num_2 = 9
        response = self.app.get(f'/mul-sum/{num_1}/{num_2}')
        self.assertIn('404', response.status)


if __name__ == '__main__':
    unittest.main()
