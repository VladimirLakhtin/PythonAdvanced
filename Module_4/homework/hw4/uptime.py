"""
Напишите GET-эндпоинт /uptime, который в ответ на запрос будет выводить строку вида f"Current uptime is {UPTIME}",
где UPTIME — uptime системы (показатель того, как долго текущая система не перезагружалась).

Сделать это можно с помощью команды uptime.
"""
import os
import re

from flask import Flask
import subprocess

app = Flask(__name__)


@app.route("/uptime", methods=['GET'])
def uptime() -> str:
    uptime_info = subprocess.run(
        [r'C:\Windows\System32\bash.exe', '-c', 'uptime'],
        capture_output=True
    )
    UPTIME = re.findall(r'up (.+),  \d+ users',
                        uptime_info.stdout.decode('ascii'))[0]
    return f'Current uptime is {UPTIME}'


if __name__ == '__main__':
    app.run(debug=True)
