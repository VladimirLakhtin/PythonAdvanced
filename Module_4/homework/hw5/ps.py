"""
Напишите GET-эндпоинт /ps, который принимает на вход аргументы командной строки,
а возвращает результат работы команды ps с этими аргументами.
Входные значения эндпоинт должен принимать в виде списка через аргумент arg.

Например, для исполнения команды ps aux запрос будет следующим:

/ps?arg=a&arg=u&arg=x
"""
import shlex, subprocess

from flask import Flask, request

app = Flask(__name__)


@app.route("/ps", methods=["GET"])
def _ps() -> str:
    args = request.args.getlist('arg')
    args_str_cleaned = shlex.quote("".join(args))
    user_cmd_str = f'ps {args_str_cleaned}'
    user_cmd = shlex.split(user_cmd_str)
    result = subprocess.run(
        [r'C:\Windows\System32\bash.exe', '-c', *user_cmd],
        capture_output=True
    )

    if result.returncode != 0:
        return 'Something went wrong', 500

    output = result.stdout.decode('ascii')
    return f'<pre>{output}<pre>'


if __name__ == "__main__":
    app.run(debug=True)
