"""
Довольно неудобно использовать встроенный валидатор NumberRange для ограничения числа по его длине.
Создадим свой для поля phone. Создайте валидатор обоими способами.
Валидатор должен принимать на вход параметры min и max — минимальная и максимальная длина,
а также опциональный параметр message (см. рекомендации к предыдущему заданию).
"""
from typing import Optional

from flask_wtf import FlaskForm
from wtforms import Field
from wtforms.validators import StopValidation


def number_length(min: int, max: int, message: Optional[str] = None):

    def _number_length(form: FlaskForm, field: Field):
        if field.data not in range(min, max+1):
            raise StopValidation(message or 'Invalid phone number')

    return _number_length


class NumberLength:
    def __init__(self, min: int, max: int, message: Optional[str] = None):
        self.min = min
        self.max = max
        self.message = message

    def __call__(self, form: FlaskForm, field: Field):
        if field.data not in range(self.min, self.max+1):
            raise StopValidation(self.message or 'Invalid phone number')


class NameValidator:

    def __call__(self, form: FlaskForm, field: Field):
        if not field.data.replace(' ', '').isalpha():
            raise StopValidation('Invalid name')
