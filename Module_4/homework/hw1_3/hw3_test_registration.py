"""
Для каждого поля и валидатора в эндпоинте /registration напишите юнит-тест,
который проверит корректность работы валидатора. Таким образом, нужно проверить, что существуют наборы данных,
которые проходят валидацию, и такие, которые валидацию не проходят.
"""

import unittest
from hw1_registration import app


class RegistrationTestCase(unittest.TestCase):

    def setUp(self) -> None:
        app.config['TESTING'] = True
        app.config["WTF_CSRF_ENABLED"] = False
        self.app = app.test_client()
        self.data = {
            'email': 'vllakhtin@gmail.com',
            'phone': 9184446529,
            'name': 'Vladimir',
            'address': 'Red 21, 53',
            'index': 24,
            'comment': 'Hi',
        }

    def test_pass_valid_data(self):
        response = self.app.post('/registration', data=self.data)
        self.assertIn('200', response.status)

    def test_validate_email(self):
        valid_emails = ['vllakhtin@gmail.com', 'anna@example.ru']
        for email in valid_emails:
            with self.subTest(email=email):
                self.data['email'] = email
                response = self.app.post('/registration', data=self.data)
                self.assertIn('200', response.status)

        invalid_emails = ['vllakhtingmail.com', 'vllakhtin@gmailcom', '', 'vllakhtin@@gmail.com', '@gmail.com']
        for email in invalid_emails:
            with self.subTest(email=email):
                self.data['email'] = email
                response = self.app.post('/registration', data=self.data)
                self.assertIn('400', response.status)

        with self.subTest(email=None):
            del self.data['email']
            response = self.app.post('/registration', data=self.data)
            self.assertIn('400', response.status)

    def test_validate_phone_number(self):
        valid_numbers = [1234567890, 1000000000, 9999999999]
        for number in valid_numbers:
            with self.subTest(number=number):
                self.data['phone'] = number
                response = self.app.post('/registration', data=self.data)
                self.assertIn('200', response.status)

        invalid_numbers = [12345678901, 123456789]
        for number in invalid_numbers:
            with self.subTest(number=number):
                self.data['phone'] = number
                response = self.app.post('/registration', data=self.data)
                self.assertIn('400', response.status)

        with self.subTest(number=None):
            del self.data['phone']
            response = self.app.post('/registration', data=self.data)
            self.assertIn('400', response.status)

    def test_validate_name(self):
        valid_names = ['Vladimir Leonidovich', 'Anna', 'Mandik Alina Denisovna']
        for name in valid_names:
            with self.subTest(name=name):
                self.data['name'] = name
                response = self.app.post('/registration', data=self.data)
                self.assertIn('200', response.status)

        invalid_names = [123, '123', 'Vladimir1 Lakhin', 'V1']
        for name in invalid_names:
            with self.subTest(name=name):
                self.data['name'] = name
                response = self.app.post('/registration', data=self.data)
                self.assertIn('400', response.status)

        with self.subTest(name=None):
            del self.data['name']
            response = self.app.post('/registration', data=self.data)
            self.assertIn('400', response.status)

    def test_cannot_pass_request_without_address(self):

        del self.data['address']
        response = self.app.post('/registration', data=self.data)
        self.assertIn('400', response.status)

    def test_cannot_pass_request_without_index(self):

        del self.data['index']
        response = self.app.post('/registration', data=self.data)
        self.assertIn('400', response.status)

    def test_can_pass_request_without_comment(self):

        del self.data['comment']
        response = self.app.post('/registration', data=self.data)
        self.assertIn('200', response.status)



if __name__ == '__main__':
    unittest.main()
