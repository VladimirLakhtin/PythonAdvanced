import getpass
import logging
import string
from typing import Iterable

import hashlib
import re


loger = logging.getLogger()


def is_strong_password(password: str) -> tuple[bool, str]:
    password_set = set(password)
    symbols_set = set('!@#$%^&*()-+=_')

    if len(password) < 8:
        msg = 'Password must consist of at least 8 characters'
        return False, msg

    if not password_set & set(string.ascii_lowercase):
        msg = 'Password must contains at least 1 lowercase letter'
        return False, msg

    if not password_set & set(string.ascii_uppercase):
        msg = 'Password must contains at least 1 uppercase letter'
        return False, msg

    if not password_set & set('1234567890'):
        msg = 'Password must contains at least 1 digit'
        return False, msg

    if not password_set & symbols_set:
        msg = f'Password must contains at least 1 special symbol: {"".join(symbols_set)}'
        return False, msg

    loger.debug(f'Password: {password}')
    password_words = re.findall(r'[a-z]+', password.lower())
    loger.debug(f'Words in password: {password_words}')
    for word in password_words:
        if word in words:
            msg = 'Password must not contains english words'
            return False, msg
    return True, 'Ok'


def input_and_check_password():
    loger.debug("Start input_and_check_password")
    password = getpass.getpass()

    if not password:
        loger.warning("You enter blank password")
        return False

    is_strong, msg = is_strong_password(password)
    if not is_strong:
        loger.warning(msg)
        return False

    try:
        hasher = hashlib.md5()
        hasher.update(password.encode('latin-1'))

        if hasher.hexdigest() == '098jgdksklsdfjanjskfhuaij':
            loger.info("You are authenticated")
            return True

    except ValueError:
        loger.warning("You enter incorrect password")

    loger.warning("You enter wrong password")
    return False


if __name__ == '__main__':

    logging.basicConfig(
        level=logging.DEBUG,
        filename='stderr.txt',
        filemode='a',
        format='%(asctime)s [%(levelname)s] %(name)s: %(message)s',
        datefmt='%H:%M:%S'
    )

    with open('words.txt') as file:
        words = set(file.read().split('\n'))
        loger.debug('Words was loaded')

    loger.info("You trying to authenticate in Skillbox")
    count_number = 999
    loger.info(f"You have {count_number} attempts")

    while count_number > 0:
        if input_and_check_password():
            exit(0)
        count_number -= 1

    loger.error(f"User {count_number} time entered wrong password")
    exit(1)

