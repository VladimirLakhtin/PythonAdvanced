import getpass
import logging

import hashlib

loger = logging.getLogger()


def input_and_check_password():
    loger.debug("Start input_and_check_password")
    password = getpass.getpass()

    if not password:
        loger.warning("You enter blank password")
        return False

    try:
        hasher = hashlib.md5()
        hasher.update(password.encode('latin-1'))

        if hasher.hexdigest() == '098jgdksklsdfjanjskfhuaij':
            loger.info("You are authenticated")
            return True

    except ValueError:
        loger.warning("You enter wrong password")

    return False


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.DEBUG,
        filename='stderr.txt',
        filemode='a',
        format='%(asctime)s [%(levelname)s] %(name)s: %(message)s'
    )
    loger.info("You trying to authenticate in Skillbox")
    count_number = 3
    loger.info(f"You have {count_number} attempts")

    while count_number > 0:
        if input_and_check_password():
            exit(0)
        count_number -= 1

    loger.error("User three time entered wrong password")
    exit(1)
