import re
from typing import Iterable


keyboard = {
    '2': ['a', 'b', 'c'],
    '3': ['d', 'e', 'f'],
    '4': ['g', 'h', 'i'],
    '5': ['j', 'k', 'l'],
    '6': ['m', 'n', 'r', 's'],
    '7': ['p', 'q', 's'],
    '8': ['t', 'u', 'v'],
    '9': ['w', 'x', 'y', 'z'],
}


def my_t9(nums: int) -> Iterable[str]:
    regex = ''.join(
        rf'[{"".join(keyboard[num])}]'
        for num in str(nums)
    )
    return re.findall(fr"\b({regex})\b[^']", words)


if __name__ == '__main__':
    with open('words.txt') as file:
        words = file.read()
    print(my_t9(223))
