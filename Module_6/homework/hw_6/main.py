from urllib.parse import urlparse

import flask
from flask import url_for, request

app = flask.Flask(__name__)


@app.route('/')
def index():
    return 'Index', 200


@app.route('/catalog')
def catalog():
    return 'Catalog', 200


@app.route('/profile')
def profile():
    return 'Profile', 200


@app.route('/cart')
def cart():
    return 'Cart', 200


def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@app.errorhandler(404)
def error_404_handler(err):
    links = []
    for rule in app.url_map.iter_rules():
        if "GET" in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links.append((url, rule.endpoint))
    o = urlparse(request.base_url)
    base_url = o.scheme + '://' + o.hostname + ':' + str(o.port)

    msg = 'This page not found. You can go to the following urls:'
    allows_urls = '<br>'.join(
        f"<a href={base_url + ''.join(link[0])}>{link[1]}</a>"
        for link in links
    )
    return msg + '<br>'*2 + allows_urls, 404


if __name__ == '__main__':
    app.debug = True
    app.run()
