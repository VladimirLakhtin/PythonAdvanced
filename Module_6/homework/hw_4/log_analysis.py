from itertools import chain, groupby
import json
import re
import subprocess
from typing import Dict


bash_path = r'C:\Windows\System32\bash.exe'
log_filename = 'skillbox_json_messages.log'
levels = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
SPLITTER = '-----------------------------------------------------------'


def count_mes_by_level() -> Dict[str, str]:
    global data
    keyfunc = lambda x: x['level']
    data = sorted(data, key=keyfunc)
    return {
        key: len(list(items))
        for key, items in groupby(data, keyfunc)
    }


def hour_for_max_logs_count():
    global data
    keyfunc = lambda x: x['time'][:2]
    data = sorted(data, key=keyfunc)
    return max([
        [key, len(list(items))]
        for key, items in groupby(data, keyfunc)
    ], key=lambda x: x[1])


def count_critical():
    proc = subprocess.run(
        ['egrep', '-c', r'"time": "17:[0-1][0-9]:[0-9]{2}", "level": "CRITICAL"', f'{log_filename}'],
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )
    return proc.stdout.decode()[:-1]


def count_mes_with_dog():
    proc = subprocess.run(
        ['egrep', '-c', '"message": "(dog .*|.* dog|.* dog .*|dog)"', f'{log_filename}'],
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )
    return proc.stdout.decode()[:-1]


def most_popular_word_in_warning():
    warning_words = list(chain(*[
        re.findall('[a-z]+', log['message'].lower())
        for log in data
        if log['level'] == 'WARNING'
    ]))
    return max(
        [[word, warning_words.count(word)] for word in set(warning_words)],
        key=lambda x: x[1]
    )


def log_analysis():
    print('SUMMARY OF LOG INFORMATION')
    print(SPLITTER)

    count_dict = count_mes_by_level()
    print("1. Count messages by level in last day:\n")
    for level in count_dict:
        print(f"{level}: {count_dict[level]}")
    print(SPLITTER)

    hour, logs_count = hour_for_max_logs_count()
    print(f"2. During {hour} hour was max logs count - {logs_count}")
    print(SPLITTER)

    critical_logs = count_critical()
    print(f"3. From 17:00:00 to 17:20:00 there were {critical_logs} CRITICAL logs")
    print(SPLITTER)

    count_logs_with_dog = count_mes_with_dog()
    print(f"4. There are {count_logs_with_dog} logs with 'dog' in message")
    print(SPLITTER)

    max_word, count = most_popular_word_in_warning()
    print(f"5. The most frequent word in WARNING logs is: {max_word} - {count}")
    print(SPLITTER)


if __name__ == '__main__':
    with open('skillbox_json_messages.log') as file:
        dicts_list = file.readlines()
        data = [json.loads(d) for d in dicts_list]

    log_analysis()
