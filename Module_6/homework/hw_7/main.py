"""
Помимо того чтобы логи писать, нужно их ещё и уметь читать,
иначе мы будем как в известном анекдоте, писателями, а не читателями.

Для вас мы написали простую функцию обхода binary tree по уровням.
Также в репозитории есть файл с логами, написанными этой программой.

Напишите функцию restore_tree, которая принимает на вход путь до файла с логами
    и восстанавливать исходное BinaryTree.

Функция должна возвращать корень восстановленного дерева

def restore_tree(path_to_log_file: str) -> BinaryTreeNode:
    pass

Примечание: гарантируется, что все значения, хранящиеся в бинарном дереве уникальны
"""
import itertools
import logging
import multiprocessing
import random
import re
from collections import deque
from dataclasses import dataclass
from typing import Optional, Dict

logger = logging.getLogger("tree_walk")


@dataclass
class BinaryTreeNode:
    val: int
    left: Optional["BinaryTreeNode"] = None
    right: Optional["BinaryTreeNode"] = None

    def __repr__(self):
        return f"<BinaryTreeNode[{self.val}]>"


def walk(root: BinaryTreeNode):
    queue = deque([root])

    while queue:
        node = queue.popleft()

        logger.info(f"Visiting {node!r}")

        if node.left:
            logger.debug(
                f"{node!r} left is not empty. Adding {node.left!r} to the queue"
            )
            queue.append(node.left)

        if node.right:
            logger.debug(
                f"{node!r} right is not empty. Adding {node.right!r} to the queue"
            )
            queue.append(node.right)


counter = itertools.count(random.randint(1, 10 ** 6))


def get_tree(max_depth: int, level: int = 1) -> Optional[BinaryTreeNode]:
    if max_depth == 0:
        return None

    node_left = get_tree(max_depth - 1, level=level + 1)
    node_right = get_tree(max_depth - 1, level=level + 1)
    node = BinaryTreeNode(val=next(counter), left=node_left, right=node_right)

    return node


def parse_log(log: str) -> Dict[str, int]:
    values = re.findall(r'[0-9]+', log)
    result = {'value': int(values[0])}
    for side in ('left', 'right'):
        if side in log:
            result[side] = int(values[1])
    return result


def restore_tree(path_to_log_file: str) -> BinaryTreeNode:
    with open(path_to_log_file) as file:
        logs = file.read().splitlines()

    root_val = parse_log(logs[0])['value']
    nodes = {root_val: BinaryTreeNode(val=root_val)}
    for log in logs[1:]:
        log_info = parse_log(log)
        node = nodes.setdefault(
            log_info['value'], BinaryTreeNode(val=log_info['value']))

        for side in ('left', 'right'):
            if side in log_info:
                val = log_info[side]
                nodes[val] = BinaryTreeNode(val=val)
                setattr(node, side, nodes[val])

    return nodes[root_val]


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(levelname)s:%(message)s",
        filename="test",
    )
    #
    # root = get_tree(7)
    # walk(root)

    root = restore_tree('walk_log_4.txt')
    walk(root)

